# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.http import request
import unicodedata

class fci_gh_eva_empleados(osv.osv):
    _name = 'fci_gh.eva_empleados'
    _description="Formulario de Empleados"
    _rec_name = "empleado_id"


    def total_evaluacion(self,cr,uid,ids,field_name,arg,context=None):
        suma = 0.0
        suma_total = {}
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')
        for atributos in self.browse(cr,uid,ids):
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line'], context=context):
                for  data in linea_eva['eva_empleados_line']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line1'], context=context):
                for  data in linea_eva['eva_empleados_line1']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line2'], context=context):
                for  data in linea_eva['eva_empleados_line2']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            suma_total[atributos.id] = suma
        return suma_total

    def total_odi(self,cr,uid,ids,field_name,arg,context=None):
        suma = 0.0
        suma_total = {}
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')
        for atributos in self.browse(cr,uid,ids):
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line'], context=context):
                for  data in linea_eva['eva_empleados_line']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            suma_total[atributos.id] = suma
        return suma_total

    def total_comp(self,cr,uid,ids,field_name,arg,context=None):
        suma = 0.0
        suma_total = {}
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')
        for atributos in self.browse(cr,uid,ids):
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line1'], context=context):
                for  data in linea_eva['eva_empleados_line1']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            suma_total[atributos.id] = suma
        return suma_total

    def total_tec(self,cr,uid,ids,field_name,arg,context=None):
        suma = 0.0
        suma_total = {}
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')
        for atributos in self.browse(cr,uid,ids):
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line2'], context=context):
                for  data in linea_eva['eva_empleados_line2']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            suma_total[atributos.id] = suma
        return suma_total

    def escala(self,cr,uid,ids,field_name,arg,context=None):
        valor_evaluar = 0.0
        escala_final = False
        val = {}
        suma = 0.0
        suma_total = {}
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')
        for atributos in self.browse(cr,uid,ids):
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line'], context=context):
                for  data in linea_eva['eva_empleados_line']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line1'], context=context):
                for  data in linea_eva['eva_empleados_line1']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line2'], context=context):
                for  data in linea_eva['eva_empleados_line2']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.subtotal
            suma_total[atributos.id] = suma

        for total_evalua in self.read(cr,uid,ids,['total_eva','tipo_evaluacion_id'],context=context):
            valor_evaluar = suma
            tipo_evaluacion =  total_evalua['tipo_evaluacion_id'][1]
            rec_obj=self.pool.get('fci_gh.rangoactuacion')
            if tipo_evaluacion != 'EMPLEADO / CONTRATADOS':
                resultado = rec_obj.search(cr, uid, [], order='rango_tecnico', context=context)
            else:
                resultado = rec_obj.search(cr, uid, [], order='rango', context=context)
                #resultado.reverse()

        for i in resultado:
            for resp in rec_obj.browse(cr,uid,i):
                if tipo_evaluacion == 'EMPLEADO / CONTRATADOS':
                    valor_rango = resp.rango

                else:
                    valor_rango = resp.rango_tecnico
                if valor_evaluar <= valor_rango:
                    escala_final = resp.escala_cualitativa.encode('utf8')
                    val[ids[0]] = escala_final
                    break
            if escala_final:
                break
        return val

    def dias_pago(self,cr,uid,ids,field_name,arg,context=None):
		valor_evaluar = 0.0
		escala_final = False
		val = {}

		for total_evalua in self.read(cr,uid,ids,['total_eva','tipo_evaluacion_id'],context=context):
			valor_evaluar = total_evalua['total_eva']
			tipo_evaluacion =  total_evalua['tipo_evaluacion_id'][1]


		rec_obj=self.pool.get('fci_gh.rangoactuacion')
		resultado = rec_obj.search(cr, uid, [], context=context)
		resultado.reverse()
		for i in resultado:
			for resp in rec_obj.browse(cr,uid,i):
				if tipo_evaluacion == 'EMPLEADO / CONTRATADOS':
					valor_rango = resp.rango
				else:
					valor_rango = resp.rango_tecnico

				if valor_evaluar <= valor_rango:
					escala_final = resp.dias_salario_integral
					val[ids[0]] = escala_final
					break
			if escala_final:
				break
		return val



    _columns={
        'empleado_id':fields.many2one(
                    'hr.employee',
                    'Empleado',
                    required=True,
                    ),
        'departamento':fields.related(
                    'empleado_id',
                    'department_id',
                    string='Departamento',
                    type="many2one",
                    relation="hr.department",
                    readonly=True,
                    help='Departamento donde esta ubicado el empleado',
                    ),
        'cedula_identidad':fields.char(
					string='Cédula de Identidad',
					readonly=True,
					related='empleado_id.identification_id'
					),
        'work_email':fields.char(
					string='Correo',
					readonly=False,
					related='empleado_id.work_email',
					required=True
					),
        'evaluador':fields.related(
                    'empleado_id',
                    'coach_id',
                    string='Evaluador',
                    type="many2one",
                    relation="hr.employee",
                    readonly=True,
                    #store = True,
                    help='Persona que evaluará',
                    ),
        'director':fields.related(
                    'empleado_id',
                    'parent_id',
                    string='Director',
                    type="many2one",
                    relation="hr.employee",
                    readonly=True,
                    help='Director del Departamento',
                    ),
        'tipo_cargo':fields.related(
                    'empleado_id',
                    'cargo_id',
                    string='Tipo de Cargo',
                    type="many2one",
                    relation="fci_gh.con_cargo",
                    readonly=True,
                    help='Tipo del Cargo',
                    ),
        'tipo_evaluacion':fields.char(
                    'Tipo de Evaluacion',
                    size=50,
                    required=False,
                    help='Tipo de Evaluacion',
         ),
         'tipo_evaluacion_id':fields.many2one(
                    'fci_gh.con_tipoevalucion',
                    'Tipo de Evaluación id',
                    required=False,
                    help='Tipo de Evaluacion id',
         ),
        'eva_empleados_line':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id',
                    'ODI',
                    ),
        'configfecha_id':fields.many2one(
                    'fci_gh.configfecha',
                    'Ciclo de Evaluación',
                    required=True,

                    ),
        'state': fields.selection([
                    ('pre_inicio', 'Inicio de URE'),
                    ('inicio', 'Inicio de Evaluacion'),
                    ('recibida', 'Evaluacion Recibida'),
                    ('revisada', 'Evaluacion Revisada'),
                    ('enviada', 'Evaluacion Enviada'),
                    ('validada', 'Evaluacion Validada'),
                    ], 'Estatus',
                    readonly=True,
                    copy=False,
                    help="Estado en el que se encuentra actualmente la evaluacion.",
                    select=True),
         'eva_empleados_line':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id',
                    'ODI',
                    ),
         'eva_empleados_line1':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id1',
                    'Competenciass',
                    ),
         'eva_empleados_line2':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id2',
                    'Competencias Técnico',
                    ),
        'eva_empleados_line3':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id3',
                    'Criterios',
                    ),
        'eva_empleados_line4':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id4',
                    'Logros',
                    ),
        'eva_empleados_line5':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id5',
                    'Impacto',
                    ),
        'eva_empleados_line6':fields.one2many(
                    'fci_gh.eva_empleados.line',
                    'eva_empleados_line_id6',
                    'Institucional',
                    ),
         'total_odi':fields.function(
                    total_odi,
                    string='Sub-Total ODI',
                    type='float',
                    store=True,
                    ),
         'total_comp':fields.function(
                    total_comp,
                    string='Sub-Total Competencias',
                    type='float',
                    store=True,
                    ),
         'total_tec':fields.function(
                    total_tec,
                    string='Sub-Total Competencias',
                    type='float',
                    store=True,
                    ),
         'total_eva':fields.function(
                    total_evaluacion,
                    string='Total Evaluación',
                    type='float',
                    store=True,
                    ),
         'actuacion':fields.function(
                    escala,
                    string='Rango Actuación',
                    type="text",
                    store=True,
                    ),
         'dias_salario':fields.function(
                    dias_pago,
                    string='Dias Salariales',
                    type="integer",
                    store=True,
                    ),
        'recomen_odi':fields.text(
                    'RECOMENDACIONES Y/O CAPACITACIONES SUGERIDAS POR EL EVALUADOR:',
                    size=250,
                    required=False,
                    ),
        'observ_odi':fields.text(
                    'OBSERVACIONES DEL EVALUADO:',
                    size=250,
                    required=False,
                    ),
        'recomen_comp':fields.text(
                    'RECOMENDACIONES Y/O CAPACITACIONES SUGERIDAS POR EL EVALUADOR:',
                    size=250,
                    required=False,
                    ),
        'observ_comp':fields.text(
                    'OBSERVACIONES DEL EVALUADO:',
                    size=250,
                    required=False,
                    ),
        'recomen_tec':fields.text(
                    'RECOMENDACIONES Y/O CAPACITACIONES SUGERIDAS POR EL EVALUADOR:',
                    size=250,
                    required=False,
                    ),
        'observ_tec':fields.text(
                    'OBSERVACIONES DEL EVALUADO:',
                    size=250,
                    required=False,
                    ),

    }

    _sql_constraints = [
        ('evaluacion_uniq',
        'unique(empleado_id,configfecha_id)',
        'Solo una evaluacion'),
    ]

    _defaults = {
        'state':'inicio',
    }

    #~ def imprimir_reporte(self,cr,uid,ids,context=None):
        #~ values = {
            #~ 'ids': ids,
            #~
            #~ 'model': 'fci_gh.eva_empleados',
            #~ 'form': self.read(cr,uid,ids,context=context)[0] #trae un diccionario con todos los datos de ese objeto, de acuerdo al id
        #~ }
        #~ for i in ids:
                    #~ print i
        #~ return self.pool['report'].get_action(cr,uid,[],'fci_gh_evaluacion.id_template_mesa_qweb',data=values, context=context)


    def reversar_validacion_evaluacion(self,cr,uid,ids,context=None):
        for i in self.browse(cr,uid,ids):
            id_eva = i.id
        self.write(cr,uid,id_eva,{'state':'inicio'},
                    context=context)

    def reenviar_correo(self,cr,uid,ids,context=None):
        mail_template='email_template_prueba'
        ir_model_data_obj = self.pool.get('ir.model.data')
        print 'bien 1'
        template_id = ir_model_data_obj.get_object_reference(
            cr,
            uid,
            'fci_gh_evaluacion',
            mail_template)[1]
        ctx = dict()
        ctx.update({

                    'default_composition_mode': 'email',
                    'default_subject': '',
                    'mark_so_as_sent': True,
                    'actuacion':'bIen',
        })
        self.pool.get('email.template').send_mail(cr, uid, template_id, ids[0], True, context=ctx)

    def reversar_envio(self,cr,uid,ids,context=None):
        for i in self.browse(cr,uid,ids):
            id_eva = i.id
        self.write(cr,uid,id_eva,{'state':'inicio'},
                    context=context)

    def enviar_evaluacion(self,cr,uid,ids,vals,context=None):
        cr, uid, context = request.cr, request.uid, request.context
        suma = 0.00
        suma2 = 0.00
        verifica = False
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')
        for i in self.browse(cr,uid,ids):
            id_eva = i.id

        tipo_evaluacion =self.search(cr,uid,
                        [('tipo_evaluacion','=','EMPLEADO / CONTRATADOS')],
                        context=context)

        for aux in tipo_evaluacion:
            if aux == id_eva:
                verifica = True

        if verifica:
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line'], context=context):
                for  data in linea_eva['eva_empleados_line']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.peso
            if suma < 50:
                 raise osv.except_osv(
                        ('Error!'),
                        (u'Debes Cargar Los Objetivos Completos'))
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line1'], context=context):
                for  data in linea_eva['eva_empleados_line1']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma2 = suma2 + i.peso
            if suma2 < 50:
                 raise osv.except_osv(
                        ('Error!'),
                        (u'Debes Cargar Las Copetencias Completas'))
        self.write(cr,uid,id_eva,{'state':'recibida'},
                    context=context)
        return {
         'type': 'ir.actions.act_window',
         'name': ('Empleados'),
         'res_model': 'hr.employee',
         'view_type': 'form',
         'view_mode': 'tree,form',
         'target': 'current',
               }

    def validar_evaluacion(self,cr,uid,ids,context=None):
        suma = 0.00
        suma2 = 0.00
        verifica = False
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')
        for i in self.browse(cr,uid,ids):
            id_eva = i.id

        tipo_evaluacion =self.search(cr,uid,
                        [('tipo_evaluacion','=','EMPLEADO / CONTRATADOS')],
                        context=context)

        for aux in tipo_evaluacion:
            if aux == id_eva:
                verifica = True

        if verifica:
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line'], context=context):
                for  data in linea_eva['eva_empleados_line']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma = suma + i.peso
            if suma < 50:
                 raise osv.except_osv(
                        ('Error!'),
                        (u'Debes Cargar Los Objetivos Completos'))
            for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line1'], context=context):
                for  data in linea_eva['eva_empleados_line1']:
                    for i in rec_obj_rec_obj.browse(cr,uid,data):
                        suma2 = suma2 + i.peso
            if suma2 < 50:
                 raise osv.except_osv(
                        ('Error!'),
                        (u'Debes Cargar Las Copetencias Completas'))

	mail_template='email_template_prueba'
	ir_model_data_obj = self.pool.get('ir.model.data')
	print 'bien 1'
	template_id = ir_model_data_obj.get_object_reference(
                                        cr,
                                        uid,
                                        'fci_gh_evaluacion',
                                        mail_template)[1]
	ctx = dict()
        ctx.update({

            'default_composition_mode': 'email',
            'default_subject': '',
            'mark_so_as_sent': True,
            'actuacion':'bIen',
        })
	self.pool.get('email.template').send_mail(cr, uid, template_id, ids[0], True, context=ctx)
        self.write(cr,uid,id_eva,{'state':'validada'},
                    context=context)


    def datos_empleado(self,cr,uid,ids,empleado_id,context=None):
        res={}
        rec_obj_rec_obj=self.pool.get('hr.employee')
        if empleado_id:
            for i in rec_obj_rec_obj.browse(cr,uid,empleado_id):
                res={
                    'departamento':i.department_id.id,
                    'evaluador':i.coach_id,
                    'director':i.parent_id,
                    'tipo_cargo':i.cargo_id.id,
                    'tipo_evaluacion':i.cargo_id.tipoevaluacion_id.nombre_evaluacion,
                    'tipo_evaluacion_id':i.cargo_id.tipoevaluacion_id.id,
                    }
        return {'value':res}

    def verifica_peso(self,cr,uid,ids,context=None):
        suma = 0.00
        suma2 = 0.00
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')

        for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line'], context=context):
            for  data in linea_eva['eva_empleados_line']:
                for i in rec_obj_rec_obj.browse(cr,uid,data):
                    suma = suma + i.peso
        if suma > 50:
            return False
        return True

    def verifica_peso2(self,cr,uid,ids,context=None):
        suma = 0.00
        rec_obj_rec_obj=self.pool.get('fci_gh.eva_empleados.line')

        for linea_eva in self.read(cr, uid, ids, ['eva_empleados_line1'], context=context):
            for  data in linea_eva['eva_empleados_line1']:
                for i in rec_obj_rec_obj.browse(cr,uid,data):
                    suma = suma + i.peso
        if suma > 50:
            return False
        return True

    _constraints = [
        (verifica_peso, 'Error!!! La suma de los Pesos no puede ser mayor a 50', ['eva_empleados_line']),
        (verifica_peso2,'Error!!! La suma de los Pesos no puede ser mayor a 50', ['eva_empleados_line1'])
    ]


class fci_gh_eva_empleados_line(osv.osv):
    _name = 'fci_gh.eva_empleados.line'
    _description="Formulario de Empleados"

    _columns={
        'actividades':fields.char(
                    'Objetivos De Desempeño Individual',
                    size=300,
                    required=False,
                    help='Coloque los objetivos de evaluacion individual del empleado',
         ),
        'actividades1':fields.char(
                    'Criterios de Innovación Considerados por el Supervisor Inmediato',
                    size=300,
                    required=False,
                    help='Coloque los Criterios de Innovación Considerados por el Supervisor Inmediato',
         ),
        'actividades2':fields.char(
                    'Logros Adicionales a los ODI alcanzados por el Trabajador',
                    size=300,
                    required=False,
                    help='Coloque los Logros Adicionales a los ODI alcanzados por el Trabajador',
         ),
        'actividades3':fields.char(
                    'Impacto de los logros generados en la Direccion, Oficina, Coordinacion',
                    size=300,
                    required=False,
                    help='Coloque los Impacto de los logros generados en la Direccion, Oficina, Coordinacion',
         ),
        'actividades4':fields.char(
                    'Institucional Generado por los Logros Adicionales a los ODI',
                    size=300,
                    required=False,
                    help='Coloque los Impactos Institucionales Generado por los Logros Adicionales a los ODI',
         ),
         'competencia_id':fields.many2one(
                    'fci_gh.con_competencia',
                    'Competencia',
                    required=False,
                    ),
        'peso':fields.float(
                    'Peso de Rangos',
                    size=4,
                    required=False,
                    help='Coloque el peso correspondiente al rango',
        ),
        'rangocualitativo_id':fields.many2one(
                    'fci_gh.rangocualitativo',
                    'Rango Cualitativo',
                    required=False,
                    ),
         'subtotal':fields.float(
                    'Sub-Total',
                    help="Subtotal de la actividad"),
         'eva_empleados_line_id':fields.many2one(
                    'fci_gh.eva_empleados',
                    'Rango Cualitativo',
                    required=False,
                    ),
         'eva_empleados_line_id1':fields.many2one(
                    'fci_gh.eva_empleados',
                    'Competencias',
                    required=False,
                    ),
         'eva_empleados_line_id2':fields.many2one(
                    'fci_gh.eva_empleados',
                    'Competencias Tecnico',
                    required=False,
                    ),
         'eva_empleados_line_id3':fields.many2one(
                    'fci_gh.eva_empleados',
                    'Criterios',
                    required=False,
                    ),
         'eva_empleados_line_id4':fields.many2one(
                    'fci_gh.eva_empleados',
                    'Logros',
                    required=False,
                    ),
         'eva_empleados_line_id5':fields.many2one(
                    'fci_gh.eva_empleados',
                    'Impacto',
                    required=False,
                    ),
         'eva_empleados_line_id6':fields.many2one(
                    'fci_gh.eva_empleados',
                    'Institucional',
                    required=False,
                    ),
    }

    def multiplica_peso(self,cr,uid,ids,peso,rango_id,context=None):
        if peso:
            if rango_id:
                rec_obj_rec_obj=self.pool.get('fci_gh.rangocualitativo')
                valor_selec = rec_obj_rec_obj.read(cr,
                                                    uid,
                                                    rango_id,
                                                    context=context)
                mult = peso * valor_selec['valor_rango']
                res={
                    'subtotal':mult,
                }
                return {'value':res}

    def multiplica_peso_tecnico(self,cr,uid,ids,rango_id,context=None):
        if rango_id:
            rec_obj_rec_obj=self.pool.get('fci_gh.rangocualitativo')
            valor_selec = rec_obj_rec_obj.read(cr,
                                                uid,
                                                rango_id,
                                                context=context)
            res={
                'subtotal':valor_selec['valor_rango_tecnico'],
            }
            return {'value':res}
