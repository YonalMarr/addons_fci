# -*- encoding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.report import report_sxw

class evaluacion_report(report_sxw.rml_parse):
    def __init__(self,cr,uid,name,context):
        super(evaluacion_report,self).__init__(cr,uid,name,context)
        #~ self.localcontext.update({
            #~ 'get_data':self.get_data,
        #~ })
        #~ self.context=context
        #~ 
    #~ def get_data(self,empleado_id):
        #~ evaluacion_obj=self.pool.get('fci_gh.eva_empleados')
        #~ tids = evaluacion_obj.search(self.cr,self.uid,[('empleado_id','=',empleado_id)])
        #~ res= evaluacion_obj.browse(self.cr,self.uid,tids)
        #~ return res
        #~ 
class report_datos_evaluaciones(osv.AbstractModel):
    _name="report.fci_gh_evaluacion.id_template_evaluacion_qweb"
    _inherit="report.abstract_report"
    _template="fci_gh_evaluacion.id_template_evaluacion_qweb"
    _wrapped_report_class = evaluacion_report
    
#~ #report_sxw.report_sxw('report.fci_gh_eva_empleados_form','fci_gh.eva_empleados','local_addons_fci/fci_gh_evaluacion/reportes/fci_gh_eva_empleados_form.rml',parser=evaluacion_report)
