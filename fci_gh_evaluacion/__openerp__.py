#-*-coding: utf-8-*-

{
    'name':'fci_gh_evaluacion',
    'version':'1.0',
    'depends':['base','base_setup','fci_gh_configuracion'],
    'author':'Solymar Quiaro',
    'category':'',
    'description':"""Sistema Para la administración de los datos de\
		             la Oficina de Gestión Humana del CFG Modulo de Evaluacion""",
    'update_xml':[],
    'data':[
		#'seguridad/fci_usu_s.xml',
		'seguridad/group_eva/ir.model.access.csv',
		'vistas/fci_gh_eva_empleados_v.xml',
        'vistas/fci_gh_eva_empleados_v_2.xml',
        'seguridad/ept_evaluaciones_f.xml',
        'vistas/fci_gh_eva_fichaempleados_r.xml',
        'reportes/fci_gh_eva_fichaempleados_rv.xml',
        #'vistas/fci_gh_eva_excelente_v.xml',
	    'datos/template_mail.xml',
    ],
    'installable':True,
    'auto_installable':False,

} 
