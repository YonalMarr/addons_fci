
from openerp.osv import osv
import time
from openerp.report import report_sxw

class fi_reportes(report_sxw.rml_parse):
    def __init__(self , cr, uid, name, context):
        super(fi_reportes,self).__init__(cr,uid,name,context)
        self.localcontext.update({
            'time':time,
            'get_data': self.get_data,
        })
        self.context = context
    
    def get_data(self):
        return 'hola mundo'

        
class reportes_fi(osv.AbstractModel):
    _name = "report.fci_archivo.fi_reporte_qweb"
    _inherit = "report.abstract_report"
    _template = "fci_archivo.fi_reporte_qweb"
    _wrapped_report_class = fi_reportes
