#-*-coding: utf-8-*-
{
    'name':'modulo_archivo',
    'version':'1.0',
    'depends':['base','base_setup','website', 'website_apiform'],
    'author':'Gerencia de Tecnología del CFG',
    'category':'',
    'description':"""Modulo de archivo""",
    'update_xml':[],
    'data':[
      #inicio 
      #'vista/primera.xml',
      'security/dc_user.xml',
      'security/operador/ir.model.access.csv',
      'vista/templates.xml',
      #'vista/login.xml',        
      #'vista/templates_inherit.xml',
      #vistas erp
      'vista/view_obpp.xml',
      'vista/view_fi.xml',
      'vista/view_prestamo.xml',
      #fi
      'vista/arc_fi.xml',
      'vista/arc_fi_proyectos.xml',
      'vista/arc_fi_detalle.xml',
      'vista/template_reporte_fi.xml',
      'reportes/reportes_fi.xml',
      
      #obpp
      'vista/arc_obpp.xml',
      'vista/arc_obpp_proyectos.xml',
      'vista/arc_obpp_detalle.xml',
      'vista/template_reporte_obpp.xml',
      'reportes/reportes_obpp.xml',

      #prestamos
      'vista/arc_busqueda_prestamo.xml',
      'vista/arc_prestamo_lista.xml',
      'vista/arc_prestamo_crear.xml',
      'vista/arc_prestamo_devolucion.xml',
      'vista/arc_prestamo_listado.xml',
      
    ],
    'installable':True,
    'auto_installable':False,

} 
