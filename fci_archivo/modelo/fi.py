# -*- uncoding: utf-8 -*-#800080
from openerp.osv import fields, osv
from openerp.http import request

class archivo_fi (osv.osv):
	_name='arc.fi'
	_rec_name=''
	
	_columns={
	
		'nombre_proyecto':fields.char('Nombre del Proyecto',size=250,help='Indique el nombre del proyecto'),
		'razon_social':fields.char('Razón social',size=250,help='Indique la razón social'),
	    'codigo_proyecto':fields.char('Código del proyecto',size=100,help='Indique el código del proyecto'),
	    'estado':fields.char('estado del proyecto',size=100,help='Indique el estado del proyecto'),
	    'rif':fields.char('Rif',size=250,help='Indique el rif del proyecto'),
	    'cedula_de_identidad':fields.boolean('Cédula de identidad',help='Tilde solo si cumple con este requisito'),
		'gaceta_municipal_juramentacion':fields.boolean('Gaceta municipal de la juramentación',help='Tilde solo si cumple con este requisito'),
		'documento_constitutivo_estatutario':fields.boolean('Documento constitutivo estatutario',help='Tilde solo si cumple con este requisito'),
		'registro_informacion_fiscal':fields.boolean('Registro información fiscal',help='Tilde solo si cumple con este requisito'),
		'certificacion_bancaria':fields.boolean('Certificación bancaria',help='Tilde solo si cumple con este requisito') ,
		'respaldo_proyecto':fields.boolean('Respaldo proyecto',help='Tilde solo si cumple con este requisito'),
		'rendicion_1':fields.boolean('Rendición 1',help='Tilde solo si cumple con este requisito'),
		'rendicion_2':fields.boolean('Rendición 2',help='Tilde solo si cumple con este requisito'),
		'rendicion_3':fields.boolean('Rendición 3',help='Tilde solo si cumple con este requisito'),
		'finiquito':fields.boolean('Finiquito',help='Tilde solo si cumple con este requisito'),
		'otros_observaciones':fields.text('Otros / observaciones',help='Esta opción es abierta para poder registrar el nombre de cualquier otra documentación u obervación'),
		'carpeta_N':fields.char('Carpeta N°',size=100,help='Indique el número de la carpeta donde se encuentra ubicado en el archivo'),
		'tomo_N':fields.char('Tomo N°',size=100,help='Indique el número de tomo donde se encuentra ubicado en el archivo'),
		'documento_N':fields.char('Documento N°',size=100,help='Indique el número de documento que corresponde'),
		'convenio_especifico_firmado':fields.boolean('Convenio específico firmado',help='Indique el número de convenio específico firmado'),
		'addendum_N':fields.boolean('Addendum N°',help='Indique el número de addendum que corresponde'),
		'convenio_marco':fields.boolean('Convenio marco',help='Indique el número de convenio marco que corresponde'),
		'convenio_marco_nombre':fields.char('Nombre de convenio marco',size=250,help='Indique el nombre del convenio marco'),

	}



