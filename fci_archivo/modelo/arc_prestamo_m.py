#-*- coding:utf-8 -*-
from openerp.osv import fields, osv 
from openerp.http import request

class arc_prestamo(osv.osv):
	_name = 'ar.archivo'

	_columns = {
		'codigo':fields.char(
						'Codigo',
						size=250, 
						required=True,
						help='Codigo del proyecto',
						),
		'departamento':fields.char(
						'Departamento',
						size=250, 
						required=True,
						help='Departamento al cual pertenece',
						),

		'responsable':fields.char(
						'Responsable', 
						size=250,
						required=True,
						help='Responsable',
						),  

		'fecha':fields.date(
						'Fecha', 
						required=True,
						help='Fecha',
						),

		'fecha_entrega':fields.date(
						'fecha_entrega', 
						help='Fecha de Entrega',
						),
		'prestado':fields.boolean(
						'prestado',
						help='Condicion del archivo',
			),
		'activo':fields.boolean('Activo'),

			
		}
