# -*- uncoding: utf-8 -*-
from openerp.osv import fields, osv
from openerp.http import request

class requisitos_obpp (osv.osv):
	_name='arc.obpp'
	_rec_name=''
	
	_columns={
		'nombre_proyecto':fields.char('Nombre',size=250, help='Indique el Nombre del Proyecto'),
		'razon_social':fields.char('Razón social',size=250,help='Indique la razón social'),
		'codigo_proyecto':fields.char('Código del Proyecto', size=100, help='Indique el Código del Proyecto. Ej. CFG-XXX-XXX'),
	    'rif':fields.char('Rif',size=50,help='Aqui se coloca el rif del proyecto'),
	    'estado':fields.char('estado',size=50,help='Aqui se coloca el estado del proyecto'),
	    ###################################################
	    'acta_constitutiva_estatutos_sociales_obpp':fields.boolean('acta constitutiva y estatutos sociales de la obpp', size=8,help='Indique el acta constitutiva y estatutos sociales de la obpp'),
		'acta_ciudadanos_fci':fields.boolean('acta de ciudadanos (as)fci'),
		'certificacion_bancaria_obpp':fields.boolean('certificacion bancaria de la obpp'),
		'rif_obpp':fields.boolean('rif de la obpp'),
		'comprobante_cita':fields.boolean('comprobante de cita') ,
		'certificado_registro_consejo_comunal':fields.boolean('certificado de registro del consejo comunal'),
		'certificado_registro_proyecto':fields.boolean('certificado de registro de proyecto',help='Tilde solo si cumple con este requisito'),
		'convenio_fci_obpp':fields.boolean('convenio fci/obpp',help='Tilde solo si cumple con este requisito'),
		'ci_5_voceros':fields.boolean('cedula de identidad(5voceros)',help='Tilde solo si cumple con este requisito'),
		'proyectos_ccm':fields.boolean('proyectos ccm',help='Tilde solo si cumple con este requisito'),
		'rendicion_n1':fields.boolean('rendicion de cuenta n1',help='Tilde solo si cumple con este requisito'),
		'rendicion_n2':fields.boolean('rendicion de cuenta n2',help='Tilde solo si cumple con este requisito'),
		'rendicion_n3':fields.boolean('rendicion de cuenta n3',help='Tilde solo si cumple con este requisito'),
		'addendum':fields.boolean('addendum',help='Tilde solo si cumple con este requisito'),
		'finiquito':fields.boolean('Finiquito',help='Tilde solo si cumple con este requisito'),
		'solicitud_prorrogas':fields.boolean('solicitud de programas',help='Tilde solo si cumple con este requisito'),
		'carpeta_N':fields.char('Carpeta', size=100, requerid=True),
		'tomo_N':fields.char('Tomo', size=100, requerid=True),
		'documento_N':fields.char('N° de Documento', size=100, requerid=True),
		'otros_observaciones':fields.text('Otros / observaciones',help='Esta opción es abierta para poder registrar el nombre de cualquier otra documentación u obervación'),

	}
