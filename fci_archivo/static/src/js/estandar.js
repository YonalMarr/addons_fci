//funcion utilizada para paginacion en /vista/arc_vista_lista_proyectos_aprovados_t.xml
jQuery(document).ready(function(){
        var id = $('#num').val();

        if(id <=11){
        min=$('#num').val();
        }else{
                min=11;
        }
  $('#paginadorTable').jTPS( {perPages:[10,20,40,'todos'],scrollStep:2,scrollDelay:10,
                 clickCallback:function (){     
                  //Cuadro de objetivos selector
                  var table = '#paginadorTable';
                   //Tienda + paginación especie en la cookie
                   document.cookie = 'jTPS=sortasc:' + $(table + ' .sortableHeader').index($(table + ' .sortAsc')) + ',' +
                                     'sortdesc:' + $(table + ' .sortableHeader').index($(table + ' .sortDesc')) + ',' +
                                      'page:' + $(table + ' .pageSelector').index($(table + ' .hilightPageSelector')) + ';';
                                }
                        });
                        //restablecer y ordenar la paginación si cookie existe
                        var cookies = document.cookie.split(';');
                        for (var ci = 0, cie = cookies.length; ci < cie; ci++) {
                                var cookie = cookies[ci].split('=');
                                if (cookie[0] == 'jTPS') {
                                        var commands = cookie[1].split(',');
                                        for (var cm = 0, cme = commands.length; cm < cme; cm++) {
                                                var command = commands[cm].split(':');
                                                if (command[0] == 'sortasc' && parseInt(command[1]) >= 0) {
                                                        $('#paginadorTable .sortableHeader:eq(' + parseInt(command[1]) + ')').click();
                                                } else if (command[0] == 'sortdesc' && parseInt(command[1]) >= 0) {
                                                        $('#paginadorTable .sortableHeader:eq(' + parseInt(command[1]) + ')').click().click();
                                                } else if (command[0] == 'page' && parseInt(command[1]) >= 0) {
                                                        $('#paginadorTable .pageSelector:eq(' + parseInt(command[1]) + ')').click();
                                                }
                                        }
                                }
                        }

                        //  mouseover para cada fila tbody y el cambio celda (td) Estilo vuelo estacionario
                        $('#paginadorTable tbody tr:not(.stubCell)').bind('mouseover mouseout',
                                function (e) {
                                        // resaltar la fila
                                        e.type == 'mouseover' ? $(this).children('td').addClass('hilightRow') : $(this).children('td').removeClass('hilightRow');
                                }
                        );

                });