# -*- coding: utf-8 -*-
import json
import logging
import base64
from cStringIO import StringIO

import openerplib

import openerp.exceptions
from werkzeug.exceptions import HTTPException
from openerp import http,tools, api,SUPERUSER_ID
from openerp.http import request
from openerp.addons.website_apiform.controladores import panel, base_tools
from datetime import datetime, date, time, timedelta

        

class ept_fi_proyectos(http.Controller):
    __HOST="10.251.4.12"
    __PORT=8079
    __DATABASE="produccion_ept"
    __LOGIN="admin"
    __PASWORD="semeolvid0"

##### PROCESOS FI  #####
    @http.route(
	    ['/fi'], 
	    type='http', auth="user", website=True)   
    def listar_proyectos_fi(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos={'parametros':{
                            'titulo':'Gestión de archivo',
                            'template':'fci_archivo.arc_fi_proyectos_busqueda',
                            }
                            }

        return panel.panel_lista(datos)


    @http.route(
        ['/fi/busqueda_c/<codigo>'], 
        type='http', auth="user", website=True)
    def listar_proyectos_fi_c(self,codigo): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context 

        estado_data={}
        estado = ''

        
        connection = openerplib.get_connection(
                                        hostname=self.__HOST,
                                        port=self.__PORT,
                                        database=self.__DATABASE,
                                        login=self.__LOGIN,
                                        password=self.__PASWORD);

        projecto_obj = connection.get_model("fi_cp.carga_proyecto") 
        conf_val_general_ids=projecto_obj.search(
                                                [('state','in',['aprobado','ejecucion','culminado','espera','adendum']),('correlativo','ilike',codigo)],
                                                )
        
        ValGeneralData = projecto_obj.read(conf_val_general_ids,[
                                                'correlativo',
                                                'nombre_proyecto',
                                                'partner_name',
                                                'estado_id',
                                                'rif',
                                                 ])

        maximo = len(ValGeneralData)
        i = 0
        while i < maximo:
            estado=ValGeneralData[i]['estado_id'][1]
            ValGeneralData[i]['estado'] = estado
            i+=1
        datos={'parametros':{
                            'titulo':'Gestión de archivo',
                            'template':'fci_archivo.arc_fi_proyectos',
                            },
                           'ValGeneralData':ValGeneralData,
                            }
        return panel.panel_lista(datos)



    @http.route(
        ['/fi/busqueda_r/<rif>'], 
        type='http', auth="user", website=True)
    def listar_proyectos_fi_r(self,rif): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context 


        print rif
        
        connection = openerplib.get_connection(
                                        hostname=self.__HOST,
                                        port=self.__PORT,
                                        database=self.__DATABASE,
                                        login=self.__LOGIN,
                                        password=self.__PASWORD); 

        projecto_obj = connection.get_model("fi_cp.carga_proyecto")
        conf_val_general_ids=projecto_obj.search(

        #conf_val_general_ids=obpp_obj.search(
                                                #[('partner_vat','ilike',''),]
                                                [('state','in',['aprobado','ejecucion','culminado','espera','adendum']),('rif','=',rif),] 


                                                )

        ValGeneralData = projecto_obj.read(conf_val_general_ids,[
                                                'correlativo',
                                                'nombre_proyecto',
                                             	'partner_name',
                                                'estado_id',
                                             	'rif',
                                                ])


        maximo = len(ValGeneralData)
        i = 0
        while i < maximo:
            estado=ValGeneralData[i]['estado_id'][1]
            ValGeneralData[i]['estado'] = estado
            i+=1

        datos={'parametros':{
	                    'titulo':'Gestión de archivo',
	                    'template':'fci_archivo.arc_fi_proyectos',
	                    },
	               		'ValGeneralData':ValGeneralData,
	                    }
        return panel.panel_lista(datos)



    @http.route(
        ['/fi/busqueda_s/<razonsocial>'], 
        type='http', auth="user", website=True)
    def listar_proyectos_fi_s(self,razonsocial): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context 


        print razonsocial
        print razonsocial
        
        connection = openerplib.get_connection(
                                        hostname=self.__HOST,
                                        port=self.__PORT,
                                        database=self.__DATABASE,
                                        login=self.__LOGIN,
                                        password=self.__PASWORD); 

        projecto_obj = connection.get_model("fi_cp.carga_proyecto")
        conf_val_general_ids=projecto_obj.search(

        #conf_val_general_ids=obpp_obj.search(
                                                #[('partner_vat','ilike',''),]
                                                [('state','in',['aprobado','ejecucion','culminado','espera','adendum']),('partner_name','ilike',razonsocial),] 


                                                )

        ValGeneralData = projecto_obj.read(conf_val_general_ids,[
                                                'correlativo',
                                                'nombre_proyecto',
                                                'partner_name',
                                                'estado_id',
                                                'rif',
                                                ])
        maximo = len(ValGeneralData)
        i = 0
        while i < maximo:
            estado=ValGeneralData[i]['estado_id'][1]
            ValGeneralData[i]['estado'] = estado
            i+=1

        datos={'parametros':{
                        'titulo':'Gestión de archivo',
                        'template':'fci_archivo.arc_fi_proyectos',
                        },
                        'ValGeneralData':ValGeneralData,
                        }
        return panel.panel_lista(datos)

        


    @http.route(['/fi/proyecto/<int:proyecto_id>'], type='http', auth="user", website=True)
    def fi_detalles(self,proyecto_id):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        estado = ''
        ##Conexion externa con get.connection 
        connection = openerplib.get_connection(
                                        hostname=self.__HOST,
                                        port=self.__PORT,
                                        database=self.__DATABASE,
                                        login=self.__LOGIN,
                                        password=self.__PASWORD); 
        ##getmodel trael el modelo de una conexion externa
        projecto_obj = connection.get_model("fi_cp.carga_proyecto")
        partner_obj = connection.get_model("res.partner")
        ValGeneralData = projecto_obj.read(proyecto_id,[
                                                'correlativo',
                                                'nombre_proyecto',
                                                'partner_name',
                                                'estado_id',
                                                'partner_id',
                                                ])
        ValGeneralData['estado']=ValGeneralData['estado_id'][1]

     

        valor_partner = partner_obj.read(ValGeneralData['partner_id'][0],[
        										'rif',
        										])

        ValGeneralData['rif'] = valor_partner['rif']

##################################################################################
        codigo = ValGeneralData['correlativo']
        fi_obj= registry.get('arc.fi')
        fi_id = fi_obj.search(cr,SUPERUSER_ID,[('codigo_proyecto', '=', codigo)],context=context)
        fi_data = fi_obj.browse(cr,SUPERUSER_ID,fi_id,context=context)
##################################################################################
        datos={'parametros':{
                        'titulo':'Gestión de archivo',
                        'template':'fci_archivo.arc_fi_detalle',
                        'url_boton_list':'/fi',
                        'remover_btn_enviar':'no',
                        'id_form':'form3',
                        },
                'datos_proyectos':ValGeneralData,
                'datos_fi':fi_data,
                  
                            }
        return panel.panel_post(datos)

    

###### PROCESOS OBPP ######

    @http.route(
        ['/obpp'], 
        type='http', auth="user", website=True)   
    def buscar_proyectos_obpp(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos={'parametros':{
                            'titulo':'Gestión de archivo',
                            'template':'fci_archivo.arc_obpp_proyectos_busqueda',
                            }
                            }
        return panel.panel_lista(datos)


    @http.route(
        ['/obpp/busqueda_c/<codigo>'], 
        type='http', auth="user", website=True)
    def listar_proyectos_obpp_c(self,codigo): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context 


        print codigo
        
        connection = openerplib.get_connection(
                                            hostname="10.251.4.11",
                                            port=8069,
                                            database="sisfci",
                                            login="pentaho.admin",
                                            password="pentaho.admin");

        obpp_obj = connection.get_model("project.project") 

        conf_val_general_ids=obpp_obj.search(
#                                                [('cfg_code','ilike',''),] 
                                                [('cfg_code','=',codigo),] 

                                                )

        obpp_data = obpp_obj.read(conf_val_general_ids,[
                                               'cfg_code',
                                                'name',
                                                'partner_name',
                                                'partner_vat',
                                                'partner_state_name'
                                                ])
       

        datos={'parametros':{
                            'titulo':'Gestión de archivo',
                            'template':'fci_archivo.arc_obpp_proyectos',
                            },
                           'ValGeneralData':obpp_data,
                            }
        return panel.panel_lista(datos)


    @http.route(
        ['/obpp/busqueda_r/<rif>'], 
        type='http', auth="user", website=True)
    def listar_proyectos_obpp_r(self,rif): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context 


        print rif
        
        connection = openerplib.get_connection(
                                            hostname="10.251.4.11",
                                            port=8069,
                                            database="sisfci",
                                            login="pentaho.admin",
                                            password="pentaho.admin");

        obpp_obj = connection.get_model("project.project") 

        conf_val_general_ids=obpp_obj.search(
                                                #[('partner_vat','ilike',''),]
                                                [('partner_vat','=',rif),] 

                                                )

        obpp_data = obpp_obj.read(conf_val_general_ids,[
                                                'cfg_code',
                                                'name',
                                                'partner_name',
                                                'partner_vat',
                                                'partner_state_name'
                                                ])

        datos={'parametros':{
                            'titulo':'Gestión de archivo',
                            'template':'fci_archivo.arc_obpp_proyectos',
                            },
                           'ValGeneralData':obpp_data,
                            }
        
    



        return panel.panel_lista(datos)



    @http.route(['/obpp/proyecto/<int:proyecto_id>'], 
        type='http', auth="user", website=True)
    def obpp_detalles(self,proyecto_id):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context


        connection = openerplib.get_connection(
                                           hostname="10.251.4.11",
                                            port=8069,
                                            database="sisfci",
                                            login="pentaho.admin",
                                            password="pentaho.admin");

        obpp_obj = connection.get_model("project.project")
        #obpp_ids = obpp_obj.search([('id','=',proyecto_id)])
        obpp_data = obpp_obj.read(proyecto_id,['cfg_code',
                                            'name',
                                            'partner_name',
                                            'partner_vat',
                                            'partner_state_name'
                                            ])
        print obpp_data
##################################################################################
        codigo = obpp_data['cfg_code']
        arc_obpp_obj= registry.get('arc.obpp')
        arc_obpp_id = arc_obpp_obj.search(cr,SUPERUSER_ID,[('codigo_proyecto', '=', codigo)],context=context)
        arc_obpp_data = arc_obpp_obj.browse(cr,SUPERUSER_ID,arc_obpp_id,context=context)
##################################################################################

        datos={'parametros':{
                        'titulo':'Gestión de archivo',
                        'template':'fci_archivo.arc_obpp_detalle',
                        'url_boton_list':'/obpp',
                        'remover_btn_enviar':'no',
                        'id_form':'form3',
                        },
                'datos_proyectos':obpp_data,
                'datos_obpp':arc_obpp_data,
                  
                            }
        return panel.panel_post(datos)

#### PRESTAMOS #####

    @http.route(['/busqueda_proyectos_prestamos'], 
        type='http', auth="user", website=True)   
    def prestamo(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        datos={'parametros':{
                            'titulo':'Gestión de archivo',
                            'template':'fci_archivo.arc_busqueda_prestamo',
                            }
                            }

        return panel.panel_lista(datos)


    @http.route(['/prestamo_proyectos/<buscar>'], 
        type='http', auth="user", website=True)   
    def lista_proyecto_prestamo(self, buscar):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos={}
        codigo = ''
        id_arc = ''
        print buscar

        fi_obj= registry.get('arc.fi')
        fi_ids = fi_obj.search(cr,SUPERUSER_ID,['|',('nombre_proyecto','ilike',buscar),('rif','ilike',buscar)],context=context)
        fi_data = fi_obj.read(cr,SUPERUSER_ID,fi_ids,['codigo_proyecto',
                                                'nombre_proyecto',
                                                'carpeta_N',
                                                'documento_N',
                                                'tomo_N',],context=context)

        fi_ids_2 = fi_obj.search(cr,SUPERUSER_ID,['|',('codigo_proyecto','ilike',buscar),('razon_social','ilike',buscar)],context=context)
        fi_data_2 = fi_obj.read(cr,SUPERUSER_ID,fi_ids_2,['codigo_proyecto',
                                                'nombre_proyecto',
                                                'carpeta_N',
                                                'documento_N',
                                                'tomo_N',],context=context)
        fi_data.extend(fi_data_2)

        maximo = len(fi_data)
        i = 0
        while i < maximo:
            fi_data[i]['tipo'] = 'FI'
            i += 1


        obpp_obj= registry.get('arc.obpp')
        obpp_ids = obpp_obj.search(cr,SUPERUSER_ID,['|',('nombre_proyecto','ilike',buscar),('rif','ilike',buscar)],context=context)
        obpp_data = obpp_obj.read(cr,SUPERUSER_ID,obpp_ids,['codigo_proyecto',
                                                'nombre_proyecto',
                                                'carpeta_N',
                                                'documento_N',
                                                'tomo_N',],context=context)
        

        obpp_ids_2 = obpp_obj.search(cr,SUPERUSER_ID,['|',('codigo_proyecto','ilike',buscar),('razon_social','ilike',buscar)],context=context)
        obpp_data_2 = obpp_obj.read(cr,SUPERUSER_ID,obpp_ids_2,['codigo_proyecto',
                                                'nombre_proyecto',
                                                'carpeta_N',
                                                'documento_N',
                                                'tomo_N',],context=context)
        obpp_data.extend(obpp_data_2)
        maximo = len(obpp_data)
        i = 0
        while i < maximo:
            obpp_data[i]['tipo'] = 'OBPP'
            i += 1




        fi_data.extend(obpp_data)

        arc_obj= registry.get('ar.archivo')

        for i in fi_data:
            codigo = i['codigo_proyecto']
            arc_ids = arc_obj.search(cr,SUPERUSER_ID,[('codigo', '=', codigo)],context=context)
            arc_data = arc_obj.read(cr,SUPERUSER_ID,arc_ids,['prestado'],context=context)
            if arc_data:
                rev_ids = arc_obj.search(cr,SUPERUSER_ID,[('codigo', '=', codigo),('activo','=',True)],context=context)
                rev_data = arc_obj.read(cr,SUPERUSER_ID,rev_ids,['prestado'],context=context)
                if rev_data[0]['prestado']:
                    i['prestado'] = 1
                else:
                    i['prestado'] = 0
            else:
                i['prestado'] = 0



        datos={'parametros':{
                        'titulo':'Gestión de archivo',
                        'template':'fci_archivo.arc_prestamo_lista',
                        },
                'datos_prestamo':fi_data,
                  
                            }
        return panel.panel_post(datos)

        #return panel.panel_post(datos)

    @http.route(['/prestamo_proyectos/listado'], 
            type='http', auth="user", website=True)
    def listar_prestamos(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos={}
        ## registry.get permite realizar la conexion a una bd local 
        prestamo_obj= registry.get('ar.archivo')
        ##Busca todos los id que encuentre Search
        prestamo_ids = prestamo_obj.search(cr,SUPERUSER_ID,[('activo','=',True)],context=context)
        ##buscando todas las data que esta en prestamo_obj con todos los los prestamos_id browse
        prestamo_data = prestamo_obj.browse(cr,SUPERUSER_ID,prestamo_ids,context=context)


        datos={'parametros':{
                        'titulo':'Gestión de archivo',
                        'template':'fci_archivo.arc_prestamo_listado',
                        },
                'datos_prestamo':prestamo_data,
                  
                            }
        return panel.panel_post(datos)





   
