# -*- coding: utf-8 -*-

import json
import logging
import base64
from cStringIO import StringIO

import openerp.exceptions
from openerp.addons.web.controllers import main
from werkzeug.exceptions import HTTPException
from openerp import http,tools, api,SUPERUSER_ID
from openerp.http import request
from openerp.addons.website_apiform.controladores import panel, base_tools
from datetime import datetime, date, time, timedelta


_logger = logging.getLogger(__name__)


class principal(main.Home):
    
    @http.route('/', auth='user', website=True)

    def index(self):
        datos={'parametros':{
                    'titulo':'Consejo Federal de Gobierno - Bienvenidos',
                    'template':'fci_archivo.primera',
                    'url_boton_list':'',
                    'css':'info',
                    },                   
                    
                    }
        return panel.panel_post(datos)


class ept_cp_carga_proyecto_controlador(http.Controller):
    
    @http.route('/inicio', auth='user', website=True)
    def archivo(self):
        datos={'parametros':{
                    'titulo':'Gestión de archivo',
                    'template':'fci_archivo.index',
                    'url_boton_list':'',
                    'css':'info',
                    },                   
                    
                    }
        return panel.panel_post(datos)
      
    
###INDEX
    @http.route('/rrhh', auth='user', website=True)
    def plan_inveraion_redirec(self):
        cr, uid, context = request.cr, request.uid, request.context
        menu_obj = request.registry['ir.ui.menu']
        menu_id=menu_obj.search(cr,SUPERUSER_ID,[('name','=','Employees')])
        url='/web#view_type=kanban&model=hr.employee&menu_id=%d' % menu_id[0]
        return request.redirect(url)
    
    @http.route('/salir', auth='user', website=True)
    def exit(self):
        cr, uid, context = request.cr, request.uid, request.context
        url='/web/session/logout?redirect=/'
        return request.redirect(url)
###FI

    @http.route(
            ['/fi/proyecto/guardar'],
            type='http', auth='user', website=True)
    def accion_crear2(self,**post): 
        

        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        res_users_obj = registry.get('arc.fi')
        ir_action_obj = registry.get('ir.actions.actions')        
        
        
        
        value={
            'lang': u'es_VE',
            'tz':'America/Caracas',
            'nombre_proyecto': post['nombre_proyecto'],
            'razon_social': post['razon_social'],
            'codigo_proyecto': post['codigo_proyecto'],
            'estado': post['estado'],
            'rif': post['rif'],
            'cedula_de_identidad': False,
            'respaldo_proyecto': False,
            'gaceta_municipal_juramentacion': False,
            'rendicion_1': False,
            'documento_constitutivo_estatutario': False,
            'rendicion_2': False,
            'registro_informacion_fiscal':False,
            'rendicion_3': False,           
            'certificacion_bancaria': False,           
            'finiquito': False,
            'convenio_especifico_firmado': False,
            'addendum_N': False,
            'otros_observaciones': post['otros_observaciones'],
            'carpeta_N': post['carpeta_N'],
            'tomo_N': post['tomo_N'],
            'documento_N': post['documento_N'],
            'convenio_marco': False,
            'convenio_marco_nombre':post['convenio_marco_nombre'],
            }

        if post.has_key('cedula_de_identidad'):
            value['cedula_de_identidad']=True
        else:
            value['cedula_de_identidad']=False


        if post.has_key('respaldo_proyecto'):
            value['respaldo_proyecto']=True
        else:
            value['respaldo_proyecto']=False


        if post.has_key('gaceta_municipal_juramentacion'):
            value['gaceta_municipal_juramentacion']=True
        else:
            value['gaceta_municipal_juramentacion']=False

        if post.has_key('rendicion_1'):
            value['rendicion_1']=True
        else:
            value['rendicion_1']=False

        if post.has_key('documento_constitutivo_estatutario'):
            value['documento_constitutivo_estatutario']=True
        else:
            value['documento_constitutivo_estatutario']=False

        if post.has_key('rendicion_2'):
            value['rendicion_2']=True
        else:
            value['rendicion_2']=False

        if post.has_key('registro_informacion_fiscal'):
            value['registro_informacion_fiscal']=True
        else:
            value['registro_informacion_fiscal']=False

        if post.has_key('rendicion_3'):
            value['rendicion_3']=True
        else:
            value['rendicion_3']=False

        if post.has_key('certificacion_bancaria'):
            value['certificacion_bancaria']=True
        else:
            value['certificacion_bancaria']=False

        if post.has_key('finiquito'):
            value['finiquito']=True
        else:
            value['finiquito']=False

        if post.has_key('convenio_especifico_firmado'):
            value['convenio_especifico_firmado']=True
        else:
            value['convenio_especifico_firmado']=False
     
        if post.has_key('addendum_N'):
            value['addendum_N']=True
        else:
            value['addendum_N']=False

        if post.has_key('convenio_marco'):
            value['convenio_marco']=True
        else:
            value['convenio_marco']=False

        codigo =  value['codigo_proyecto']
        fi_obj= registry.get('arc.fi')
        fi_id = fi_obj.search(cr,SUPERUSER_ID,[('codigo_proyecto', '=', codigo)],context=context)
        #fi_data = fi_obj.browse(cr,SUPERUSER_ID,fi_id,context=context)
        codigo_proy=fi_obj.read(cr, uid, fi_id, ['codigo_proyecto'], context=None)
        
        if codigo_proy:
            user_id = res_users_obj.write(cr, SUPERUSER_ID, fi_id, value, context=context)
        else:
            user_id = res_users_obj.create(cr, SUPERUSER_ID, value, context=context)


###OBPP


    @http.route(
            ['/obpp/proyecto/guardar'],
            type='http', auth='user', website=True)
    def accion_guardar_obpp(self,**post): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        res_users_obj = registry.get('arc.obpp')
        ir_action_obj = registry.get('ir.actions.actions')        

        value={
            'lang': u'es_VE',
            'tz':'America/Caracas',
            'nombre_proyecto': post['nombre_proyecto'],
            'razon_social': post['razon_social'],
            'codigo_proyecto': post['codigo_proyecto'],
            'rif': post['rif'],

            'estado': post['estado'],
            'acta_constitutiva_estatutos_sociales_obpp': False,
            'acta_ciudadanos_fci': False,
            'certificacion_bancaria_obpp': False,
            'rif_obpp': False,
            'comprobante_cita': False,
            'certificado_registro_consejo_comunal': False,
            'certificado_registro_proyecto':False,
            'convenio_fci_obpp': False,           
            'ci_5_voceros': False,           
            'proyectos_ccm': False,
            'rendicion_n1': False,
            'rendicion_n2': False,
            'rendicion_n3': False,
            'addendum':False,
            'finiquito': False,           
            'solicitud_prorrogas': False,           
            'otros_observaciones': post['otros_observaciones'],
            'carpeta_N': post['carpeta_N'],
            'tomo_N': post['tomo_N'],
            'documento_N': post['documento_N'],

            }

        if post.has_key('acta_constitutiva_estatutos_sociales_obpp'):
            value['acta_constitutiva_estatutos_sociales_obpp']=True
        else:
            value['acta_constitutiva_estatutos_sociales_obpp']=False

        if post.has_key('acta_ciudadanos_fci'):
            value['acta_ciudadanos_fci']=True
        else:
            value['acta_ciudadanos_fci']=False

        if post.has_key('certificacion_bancaria_obpp'):
            value['certificacion_bancaria_obpp']=True
        else:
            value['certificacion_bancaria_obpp']=False

        if post.has_key('rif_obpp'):
            value['rif_obpp']=True
        else:
            value['rif_obpp']=False

        if post.has_key('comprobante_cita'):
            value['comprobante_cita']=True
        else:
            value['comprobante_cita']=False

        if post.has_key('certificado_registro_consejo_comunal'):
            value['certificado_registro_consejo_comunal']=True
        else:
            value['certificado_registro_consejo_comunal']=False

        if post.has_key('certificado_registro_proyecto'):
            value['certificado_registro_proyecto']=True
        else:
            value['certificado_registro_proyecto']=False

        if post.has_key('convenio_fci_obpp'):
            value['convenio_fci_obpp']=True
        else:
            value['convenio_fci_obpp']=False

        if post.has_key('ci_5_voceros'):
            value['ci_5_voceros']=True
        else:
            value['ci_5_voceros']=False

        if post.has_key('proyectos_ccm'):
            value['proyectos_ccm']=True
        else:
            value['proyectos_ccm']=False

        if post.has_key('rendicion_n1'):
            value['rendicion_n1']=True
        else:
            value['rendicion_n1']=False

        if post.has_key('rendicion_n2'):
            value['rendicion_n2']=True
        else:
            value['rendicion_n2']=False

        if post.has_key('rendicion_n3'):
            value['rendicion_n3']=True
        else:
            value['rendicion_n3']=False

        if post.has_key('addendum'):
            value['addendum']=True
        else:
            value['addendum']=False

        if post.has_key('solicitud_prorrogas'):
            value['solicitud_prorrogas']=True
        else:
            value['solicitud_prorrogas']=False
                        
        codigo =  value['codigo_proyecto']
        fi_obj= registry.get('arc.obpp')

        fi_id = fi_obj.search(cr,SUPERUSER_ID,[('codigo_proyecto', '=', codigo)],context=context)

        codigo_proy=fi_obj.read(cr, uid, fi_id, ['codigo_proyecto'], context=None)
        
        if codigo_proy:
            user_id = res_users_obj.write(cr, SUPERUSER_ID, fi_id, value, context=context)
        else:
            user_id = res_users_obj.create(cr, SUPERUSER_ID, value, context=context)

    
####PRESTAMOS
    


    @http.route(['/prestamo_proyectos/crear/<proyecto>'], 
            type='http', auth="user", website=True)   
    def prestamo(self,proyecto):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos_proy = {}
        datos_proy['proyecto'] = proyecto

        datos={'parametros':{
                    'titulo':'Gestión de archivo',
                    'template':'fci_archivo.arc_prestamo_crear',
                    'url_boton_list':'',
                    'css':'info',
                    'id_form':'forarchivo',
                    'id_enviar':'enviar_archivo',
                    'action':'/crear/guardar',
                    }, 
                'datos':datos_proy,
                  
                    
                    }

        return panel.panel_post(datos)

    @http.route(
            ['/prestamo_proyectos/crear/guardar'],
            type='http', auth='user', website=True)
    def accion_crear(self,**post): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        res_users_obj = registry.get('ar.archivo')
        ir_action_obj = registry.get('ir.actions.actions')        

        value={
            'lang': u'es_VE',
            'tz':'America/Caracas',
            'codigo':post['codigo'],
            'responsable': post['responsable'],
            'departamento': post['departamento'],
            'fecha': post['fechaP'],
            'prestado':True,
            'activo':True,
        }
        activo = {
            'activo':False,
        }

        codigo =  value['codigo']
        arc_obj= registry.get('ar.archivo')
        arc_id = arc_obj.search(cr,SUPERUSER_ID,[('codigo', '=', codigo)],context=context)
        
        codigo_proy=arc_obj.read(cr, uid, arc_id, ['codigo'], context=None)
        
        if codigo_proy:
            history = res_users_obj.write(cr, SUPERUSER_ID, arc_id, activo, context=context)
            user_id = res_users_obj.create(cr,SUPERUSER_ID,value,context=context)
        else:
            user_id = res_users_obj.create(cr,SUPERUSER_ID,value,context=context)

    @http.route(['/prestamo_proyectos/devolucion/<codigo>'], 
        type='http', auth="user", website=True)
    def prestamos_detalles(self,codigo):    
        
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos={}

        prestamo_obj= registry.get('ar.archivo')

        prestamo_ids = prestamo_obj.search(cr,SUPERUSER_ID,[('codigo', '=', codigo),('activo','=',True)],context=context)

        prestamo_data = prestamo_obj.browse(cr,SUPERUSER_ID,prestamo_ids,context=context)

        datos={'parametros':{
                        'titulo':'Gestión de archivo',
                        'template':'fci_archivo.arc_prestamo_devolucion',
                        },
                'datos_prestamo1':prestamo_data,
                  
                            }
        return panel.panel_post(datos)
    
    @http.route(['/prestamo_proyectos/devolucion/guardar'],type='http', auth='user', website=True)
    def accion_crear3(self,**post): 
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        res_users_obj = registry.get('ar.archivo')
        ir_action_obj = registry.get('ir.actions.actions')        


        value={
            'fecha_entrega': post['fechaE'],
            'prestado':False,
                }

        user_id = res_users_obj.write(cr, 
        SUPERUSER_ID,int(post['id']),
        value, 
        context=context)
   

    #Funcion imprimir reporte 
    @http.route(['/fi/proyecto/imprimir/<codigo>'], 
            type='http', auth="user", website=True)
    def imprimir_fi(self,codigo):
        cr, uid, context = request.cr, request.uid, request.context
        registry = http.request.registry
        reportname='fci_archivo.fi_reporte_qweb'
        entidades_obj = registry.get('arc.fi')
        entidades_id=entidades_obj.search(cr,uid,[('codigo_proyecto','=',codigo)])
        entidades_data=entidades_obj.browse(cr,uid,entidades_id)
        

        valores={
            'nombre_proyecto':entidades_data.nombre_proyecto,
            'razon_social':entidades_data.razon_social,
            'codigo_proyecto':entidades_data.codigo_proyecto,
            'rif':entidades_data.rif,
            'cedula_de_identidad':entidades_data.cedula_de_identidad,
            'gaceta_municipal_juramentacion':entidades_data.gaceta_municipal_juramentacion,
            'documento_constitutivo_estatutario':entidades_data.documento_constitutivo_estatutario,
            'registro_informacion_fiscal':entidades_data.registro_informacion_fiscal,
            'certificacion_bancaria':entidades_data.certificacion_bancaria,
            'convenio_especifico_firmado':entidades_data.convenio_especifico_firmado,
            'respaldo_proyecto':entidades_data.respaldo_proyecto,
            'rendicion_1':entidades_data.rendicion_1,
            'rendicion_2':entidades_data.rendicion_2,
            'rendicion_3':entidades_data.rendicion_3,
            'finiquito':entidades_data.finiquito,
            'addendum_N':entidades_data.addendum_N,
            'otros_observaciones':entidades_data.otros_observaciones,
            'carpeta_N':entidades_data.carpeta_N,
            'tomo_N':entidades_data.tomo_N,
            'documento_N':entidades_data.documento_N,
            'convenio_marco':entidades_data.convenio_marco,
            'convenio_marco_nombre':entidades_data.convenio_marco_nombre,

            }
        
        pdf = request.registry['report'].get_pdf(cr, uid, [], reportname, data=valores, context=context)
        pdfhttpheaders = [('Content-Type', 'application/pdf'), ('Content-Length', len(pdf))]
        response=request.make_response(pdf, headers=pdfhttpheaders)
        response.headers.add('Content-Disposition', 'attachment; filename=%s.pdf;' % codigo)

        return response

    @http.route(['/obpp/proyecto/imprimir/<codigo>'], 
            type='http', auth="user", website=True)
    def imprimir_obpp(self,codigo):
        
        cr, uid, context = request.cr, request.uid, request.context
        registry = http.request.registry
        reportname='fci_archivo.obpp_reporte_qweb'
        entidades_obj = registry.get('arc.obpp')
        entidades_id=entidades_obj.search(cr,uid,[('codigo_proyecto','=',codigo)])
        entidades_data=entidades_obj.browse(cr,uid,entidades_id)
        
        valores={
            'nombre_proyecto':entidades_data.nombre_proyecto,
            'razon_social':entidades_data.razon_social,
            'codigo_proyecto':entidades_data.codigo_proyecto,
            'rif':entidades_data.rif,
            'acta_constitutiva_estatutos_sociales_obpp':entidades_data.acta_constitutiva_estatutos_sociales_obpp,
            'acta_ciudadanos_fci':entidades_data.acta_ciudadanos_fci,
            'certificacion_bancaria_obpp':entidades_data.certificacion_bancaria_obpp,
            'rif_obpp':entidades_data.rif_obpp,
            'comprobante_cita':entidades_data.comprobante_cita,
            'certificado_registro_consejo_comunal':entidades_data.certificado_registro_consejo_comunal,
            'certificado_registro_proyecto':entidades_data.certificado_registro_proyecto,
            'convenio_fci_obpp':entidades_data.convenio_fci_obpp,
            'ci_5_voceros':entidades_data.ci_5_voceros,
            'rendicion_n1':entidades_data.rendicion_n1,
            'rendicion_n2':entidades_data.rendicion_n2,
            'rendicion_n3':entidades_data.rendicion_n3,
            'proyectos_ccm':entidades_data.proyectos_ccm,
            'addendum':entidades_data.addendum,
            'finiquito':entidades_data.finiquito,
            'solicitud_prorrogas':entidades_data.solicitud_prorrogas,
            'otros_observaciones':entidades_data.otros_observaciones,
            'carpeta_N':entidades_data.carpeta_N,
            'tomo_N':entidades_data.tomo_N,
            'documento_N':entidades_data.documento_N,

            }

        pdf = request.registry['report'].get_pdf(cr, uid, [], reportname, data=valores, context=context)
        pdfhttpheaders = [('Content-Type', 'application/pdf'), ('Content-Length', len(pdf))]
        response=request.make_response(pdf, headers=pdfhttpheaders)
        response.headers.add('Content-Disposition', 'attachment; filename=%s.pdf;' % codigo)

        return response

