#-*-coding: utf-8-*-

{
    'name':'fci_gh_configuracion',
    'version':'1.0',
    'depends':['base','base_setup', 'hr'],
    'author':'Jorge Rengifo',
    'category':'',
    'description':"""Sistema Para la administración de los datos de\
		             la Oficina de Gestión Humana del CFG""",
    'update_xml':[],
    'data':[
        'seguridad/fci_usu_s.xml',
        'seguridad/group_hr_manager/ir.model.access.csv',
        'seguridad/group_ana/ir.model.access.csv',
		'vistas/fci_gh_vistaempleado_v.xml',
		'vistas/fci_gh_rango_actuacion_v.xml',
		'vistas/fci_gh_rango_cualitativo_v.xml',
        'vistas/fci_gh_config_fecha_v.xml',
        'vistas/fci_gh_con_competencia_v.xml',
        'vistas/fci_gh_con_tipoevalucion_v.xml',
        'vistas/fci_gh_con_cargo_v.xml',
    ],
    'installable':True,
    'auto_installable':False,

} 
