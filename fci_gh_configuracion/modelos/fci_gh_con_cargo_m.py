# -*- coding: utf-8 -*-

from openerp.osv import fields, osv 
from openerp.http import request

class fci_gh_con_cargo(osv.osv):
	_name = 'fci_gh.con_cargo'
	_rec_name = 'cargo'
	_description=u"""Este módulo es para configurar los cargos de /
					 de cada uno de los Trabajadores de CFG"""
					
	_columns={
		'cargo':fields.char(
                    'Nombre del Cargo',
                    size=150,
                    required=True,
                    help='Describa el nombre que se le dará al cargo',
         ),
        'descripcion_cargo':fields.char(
					'Definición de los Cargos',
					size=100,
					required=False,
					help='Muestra una breve descripción del cargo que se\
					coloca',
        
        ),
        'tipoevaluacion_id':fields.many2one(
					'fci_gh.con_tipoevalucion',
					'Tipo de Evaluacion',
					help='Ingrese el tipo de empleo'
		),
        'active':fields.boolean(
                    'Activo',
                    help="""Si esta activo el motor lo incluira en la 
                    vista."""),
        'ip_usuario':fields.char(
                    'Ip de usuario',
                    size=15,
                    help='Indica de que ip se esta haciendo'),
	
	}
	
	_defaults={
        'active':True,
        'ip_usuario':lambda self,cr,uid,context: request.httprequest.remote_addr
    }
