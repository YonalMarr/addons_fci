# -*- codign utf-8 -*-

from openerp.osv import fields, osv
from openerp.http import request

class fci_gh_con_tipoevalucion(osv.osv):
	_name = 'fci_gh.con_tipoevalucion'
	_rec_name = 'nombre_evaluacion'
	_description = u'Modulo para la creacion de tipo de evaluacion del CFG'
		
	_columns={
		'nombre_evaluacion':fields.char(
			'tipo de evaluacion',
			size=50,
			required=True,
			help='''Introduzca el tipo de evaluacion
			Ejemplo: Obrero, Tecnico, Analista, entre otros''',
		),
		'descripcion':fields.char(
			'Descripcion de la evaluacion',
			size=100,
			required=False,
			help='De una breve descripcion del tipo de evaluacion',
			),
	}		
