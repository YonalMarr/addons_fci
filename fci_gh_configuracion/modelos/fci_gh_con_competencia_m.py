# -*- coding: utf-8 -*-

from openerp.osv import fields, osv 
from openerp.http import request

class fci_gh_con_competencia(osv.osv):
	_name = 'fci_gh.con_competencia'
	_description="Formulario para llenar competencias de empleados"
	_rec_name='descripcion'
	_columns={
        'descripcion': fields.char(
					'Competencias',
                    size=300,
                    required=True,
                    help='Descripcion de las competencias a cumplir por el evaluando',
					),
         'tipo_evaluacion_id':fields.many2one(
                    'fci_gh.con_tipoevalucion',
                    'Tipo de Evaluacion',
                    required=True,
                    ),
                    
     }
