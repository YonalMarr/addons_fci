# -*- coding: utf-8 -*-

from openerp.osv import fields, osv 
from openerp.http import request

class fci_gh_rangoactuacion(osv.osv):
	_name = 'fci_gh.rangoactuacion'
	_rec_name = 'escala_cualitativa'
	_description=u"""Este módulo es para configurar los rango de /
					Actuación de cada uno de los Trabajadores de CFG"""
					
	_columns={
		'escala_cualitativa':fields.char(
                    'Escala Cualitativa',
                    size=150,
                    required=True,
                    help='Describa el nombre que se le dará al rango',
         ),
        'rango':fields.integer(
					'Rango de Actuación Analista',
					size=5,
					required=False,
					help='El Rango numerico que se le asigna a cada \
					cualidad',
        
        ),
        'rango_tecnico':fields.integer(
					'Rango de Actuación Tecnico',
					size=5,
					required=False,
					help='El Rango numerico que se le asigna a cada \
					cualidad',
        
        ),
        'descripcion_rango':fields.text(
					'Definición de los Rangos',
					size=500,
					required=False,
					help='Muestra una breve descripción del rango que se\
					coloca',
        
        ),
        'dias_salario_integral':fields.integer(
					'Días de Salario Integral',
					size=3,
					required=True,
					help='Se indica el Día que se pagará según el rango',
        
        ),
        'active':fields.boolean(
                    'Activo',
                    help="""Si esta activo el motor lo incluira en la 
                    vista."""),
        'ip_usuario':fields.char(
                    'Ip de usuario',
                    size=15,
                    help='Indica de que ip se esta haciendo'),
	
	}
	
	_defaults={
        'active':True,
        'ip_usuario':lambda self,cr,uid,context: request.httprequest.remote_addr
    }
