# -*- coding: utf-8 -*-

from openerp.osv import fields, osv 
from openerp.http import request

class fci_gh_rangocualitativo(osv.osv):
	_name = 'fci_gh.rangocualitativo'
	_rec_name= "nombre_rango_cualitativo"
	_description=u"""Este módulo es para configurar los rango ualitativo\
					para las evaluaciones del personal del CFG"""
					
	_columns={
		'nombre_rango_cualitativo':fields.char( 
                    'Nombre del Rango Cualitativa',
                    size=150,
                    required=True,
                    help='Describa el nombre que se le dará al rango',
					),
        'valor_rango':fields.integer(
					'Valor del Rango Cualitativo',
					size=2,
					required=True,
					help='El Rango numerico que se le asigna a cada \
					cualidad',
					),
        'valor_rango_tecnico':fields.float(
					'Valor Rango para Tecnicos',
					size=4,
					required=True,
					help='El Rango numerico que se le asigna a cada \
					cualidad para tecnicos',
					),
        'active':fields.boolean(
                    'Activo',
                    help="""Si esta activo el motor lo incluira en la 
                    vista."""
                    ),
        'ip_usuario':fields.char(
                    'Ip de usuario',
                    size=15,
                    help='Indica de que ip se esta haciendo'
                    ),
	}
	
	_defaults={
        'active':True,
        'ip_usuario':lambda self,cr,uid,context: request.httprequest.remote_addr
    }
