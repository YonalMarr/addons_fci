# -*- coding: utf-8 -*-

from openerp.osv import fields, osv 
from openerp.http import request

class fci_gh_objempleado(osv.osv):
	_name = 'hr.employee'
	_inherit = 'hr.employee'
	
	_columns = {
		'fecha_ingreso':fields.date(
              'Fecha de Ingreso', 
              required=False,
              help='Fecha de Ingreso del Empleado'),
		'fecha_egreso':fields.date(
              'Fecha de egreso', 
              required=False,
              help='Fecha de Egreso del Empleado'),
		'ip':fields.char(
             'IP',
             size=15,
             help='ip del cliente de la peticion'),
		'active':fields.boolean(
             'Activo',
             help='Si esta activo el motor lo incluirá en la vista...'
             ),
		'cargo_id':fields.many2one(
			'fci_gh.con_cargo',
			'Tipo de cargo',
			help='Ingrese el tipo de cargo del empleado'),
	}
	
	_defaults={
        'active':True,
        'ip':lambda self,cr,uid,context: request.httprequest.remote_addr
    }
