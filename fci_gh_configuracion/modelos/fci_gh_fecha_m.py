# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.http import request
from datetime import datetime, date, time, timedelta

class fci_gh_configfecha(osv.osv):
	_name = 'fci_gh.configfecha'
	_rec_name = 'ciclo_de_evaluacion'
	_description = u'Modulo para la configuración de la fecha\
	 			de la evlacuión del personal del CFG'

	_columns={
		'ciclo_de_evaluacion':fields.char(
				'Ciclo de Evaluación',
				size=50,
				requiered=True,
				help='Ingrese el nombre del ciclo de evaluación',
				),
		'activo':fields.boolean(
				'Evaluación activa',
				requiered=True,
				help='Si esta activo el motor lo incluira en la\
				 busqueda',
				),
		'fecha_inicio':fields.date(
				'Fecha de inicio',
				requiered=True,
				help='Indique la fecha en la que inician las\
				 evaluiones'),
		'fecha_fin':fields.date(
				'Fecha fin',
				requiered=True,
				help='Seleccione la fecha en la que finalizan \
				las evalaciones')
	}
	
	def seleccion_activo(self,cr,uid,ids,is_activo,context=None):
				
		if is_activo :
			ciclo_ids=self.search(cr,uid,[('activo','=',True)],
					context=context)
			if len(ciclo_ids)==1:
				for i in self.browse(cr,uid,ciclo_ids):
					self.write(cr,uid,i.id,{'activo':False},
					context=context)
				print self.write(cr,uid,ids,{'activo':True},
						context=context)
