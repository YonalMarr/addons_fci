# -*- coding: utf-8 -*-
		
{
	'name': "Inventario DC",
	'summary':"Mantenga la información de los equipos de su centro de datos de manera ordenada",
	'description': """Inventario de su Centro de Datos""",
	'author': "Gerencia de Sistemas del CFG",
	'website': "www.cfg.gob.ve",
	'category': "Informatica",
	'version':'0.1',
	'depends':['base', 'base_setup', 'hr'],
	'data':[
		'security/dc_user.xml',
		'security/operador/ir.model.access.csv',
		'views/servidores_view.xml',
		'views/servidores_v_view.xml',
		'views/swicth.xml',
		'views/routers.xml',
		'views/vlan.xml',
		'views/interfaces_view.xml',
		'views/marcas_view.xml',
		'views/modelos_view.xml',
		'views/servicios_view.xml',
		'views/estatus_view.xml',
		#'views/conf_responsable_view.xml',
		#'views/conf_gestor_view.xml',
		#'views/conf_uso_view.xml',
		#'views/bd_view.xml',
		
	],
	'demo':[
	
	],
	'test':[
	],
	'installable':True,
    'auto_installable':False,
}
