# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class interface(osv.osv):
	_name='reg.interface'
	_rec_name='nombre'
	
	_columns={
		'nombre':fields.char('Interfaz',size=80,required=False,help='Nombre de la cateogria'),
		'ip':fields.char('Dirección Ip',size=80,required=False,help='Nombre de la cateogria'),
		'descripcion':fields.char('Descripcion',size=180,required=False,help='Nombre de la cateogria'),
		'active':fields.boolean('Activo',help='Si esta activo se inclurá'),
		'servidor_id':fields.many2one('reg.servidores', 'Servidor'),
		'servidor_v':fields.many2one('reg.servidores_v', 'Servidor'),
	}
	_defaults={
		'active':True,
	
	}
