# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class marca(osv.osv):
	_name='reg.responsable'
	_rec_name='nombre'
	
	_columns={
		'nombre':fields.char('Nombre',size=80,required=False,help='Nombre del responsable'),
		'apellido':fields.char('Apellido',size=80,required=False,help='Apellido del responsable'),
	}
