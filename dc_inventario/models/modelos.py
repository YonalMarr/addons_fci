# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class modelo(osv.osv):
	_name='reg.modelo'
	_rec_name='nombre'
	
	_columns={
		'nombre':fields.char('Modelo de equipo',size=80,required=False,help='Modelo de equipo'),
		'active':fields.boolean('Activo',help='Si esta activo el motor lo inclurá')
	}
	_defaults={
		'active':True,
	
	}
