# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class servicio(osv.osv):
	_name='reg.servicio'
	_rec_name='nombre'
	
	_columns={
		'nombre':fields.char('Servicio',size=80,required=False,help='Nombre del servicio'),
		'responsable':fields.char('Responsable',size=80,required=False,help='Personal responsable del servicio'),
		'descripcion':fields.char('Descripcion',size=180,required=False,help='Descripcion del servicio'),
		'active':fields.boolean('Activo',help='Si esta activo se inclurá'),
		'servidor_id':fields.many2one('reg.servidores', 'Servidor'),
		'servidor_v':fields.many2one('reg.servidores_v', 'Servidor'),
	}
	_defaults={
		'active':True,
	
	}
