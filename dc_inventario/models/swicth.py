#-*- coding:utf-8 -*-

from openerp.osv import fields, osv

class swichts(osv.osv):
	_name='reg.swicth'
	_rec_name=''

	_columns={
		'imagen': fields.binary('Referencia', help='Imagen o fotografía del swicth, tamaño limite de 1024x1024px.'),
		'nombre':fields.char('Nombre del Equipo', size=80, required=True, help='Aqui se coloca el Nombre del Equipo'),
		'piso':fields.selection([('1','1'),('2','2'),('8','8'),('9','9')], 'Piso'),
		'marca_id':fields.many2one('reg.marca', 'Marca', required=True),
		'modelo_id':fields.many2one('reg.modelo','Modelo',required=True),
		'serial':fields.char('Serial', size=30,	required=True, help='Aqui se coloca el numero de parte del repuesto'),
		'origen':fields.char('Origen', size=30, help=''),
		'destino':fields.char('Destino', size=30, help=''),
		'usuario':fields.char('Usuario', size=15),
		'clave':fields.char('Clave', size=15),
		'vlan_ids':fields.one2many('reg.vlan','vlan_id', 'Vlan')
	}