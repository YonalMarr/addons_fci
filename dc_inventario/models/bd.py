#-*- coding:utf-8 -*-

from openerp.osv import fields, osv

class swichts(osv.osv):
	_name='reg.bd'
	_rec_name='nombre_sistema'

	_columns={
		'nombre_sistema':fields.char('Nombre del Sistema', size=80, required=True, help='Aqui se coloca el Nombre del Sistema'),
		'ip':fields.char('IP de la base de datos', size=80, required=True, help='Aqui se coloca la IP de la Base de Datos'),
		'usuario':fields.char('Nombre del Usuario', size=80, required=True, help='Aqui se coloca el Nombre del Equipo'),
		'responsable_id':fields.many2one('reg.responsable','Responsable',required=False),
		'gestor_id':fields.many2one('reg.gestor','Gestor',required=False),
		'uso_id':fields.many2one('reg.uso','Uso',required=False),
	}
