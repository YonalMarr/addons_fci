#-*- coding:utf-8 -*-

from openerp.osv import fields, osv

class swichts(osv.osv):
	_name='reg.router'
	_rec_name=''

	_columns={
		'imagen': fields.binary('Referencia', help='Imagen o fotografía del swicth, tamaño limite de 1024x1024px.'),
		'nombre':fields.char('Nombre del Equipo', size=80, required=True, help='Aqui se coloca el Nombre del Equipo'),
		'piso':fields.selection([('1','1'),('2','2'),('8','8'),('9','9')], 'Piso'),
		'marca_id':fields.many2one('reg.marca', 'Marca', required=False),
		'modelo_id':fields.many2one('reg.modelo','Modelo',required=False),
		'serial':fields.char('Serial', size=30,	required=True, help='Aqui se coloca el numero de parte del repuesto'),
		'usuario':fields.char('Usuario', size=15, required=True),
		'clave':fields.char('Clave', size=15, required=True),
		'ip':fields.char('Ip', size=15, required=True),
		'frecuencia':fields.char('Frecuencia',size=30, required=True),
		'gerencia':fields.many2one('hr.department','Gerencia', required=True),
		'pass':fields.char('Clave', size=30, required=True),
	}
