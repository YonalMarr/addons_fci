# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
import time

class servidores(osv.osv):
	_name='reg.servidores_v'
	_rec_name='nombre'
	
	_columns= {
		'imagen': fields.binary(
				"Referencia",
			help="This field holds the image used as image for our customers, limited to 1024x1024px."
		),
		'contenedor':fields.many2one(
					'reg.servidores',
					'Servidor físico',
					required=True,
					help='Selecione el servidor físico que lo contiene'
		),
		'nombre':fields.char(
					'Nombre del Equipo',
					size=80,
					required=True,
					help='Aqui se coloca el Nombre del Equipo',
		),
		'tipo':fields.selection(
					[('fisico','Físico'),('virtual','Virtual'),('desktop','Desktop'),('desktopc','Desktop PC')], 'Tipo',
		),
		'interface_ids':fields.one2many(
					'reg.interface',
					'servidor_v',
					'Interface',
					required=True,
		),
		'servicio_ids':fields.one2many(
					'reg.servicio',
					'servidor_v',
					'Servicios',
					required=True,
		),
		'serial_v':fields.char(
					'Serial',
					size=30,
					required=True,
					help='Aqui se coloca el numero de parte del repuesto',
					related='contenedor.serial'
		),
		'sistema_op':fields.selection(
					[(''),('')],
					'Sistema Operativo',
					size=30,
					required=True,
					help='Aqui se coloca el numero de parte del repuesto',
					related='contenedor.sistema_op_id'        
		),
		'procesador':fields.char(
					'Procesador',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'memoria':fields.char(
					'Memoria',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'disco':fields.char(
					'Disco',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),	
		'usuario':fields.char(
					'Usuario',
					size=40,
					required=True,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'clave':fields.char(
					'Clave',
					size=40,
					required=True,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'estatus_id':fields.many2one(
					'reg.estatus',
					'Estatus',
					required=True,
		),
		'ssh':fields.selection(
					[('Si','Si'),('No','No')], 'ssh'
		),
		'puerto':fields.char(
					'Puerto',
					required=True,
					size=6,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'rack':fields.char(
					'Rack',
					size=30,
					required=False,
					help='Coloque el número del rack',
					related='contenedor.rack'
		),
		'posicion_frontal':fields.char(
					'Posición Frontal',
					size=30,
					required=False,
					help='Coloque el número del rack',
					related='contenedor.posicion_frontal'
		),
		'posicion_trasera':fields.char(
					'Posición Trasera',
					size=30,
					required=False,
					help='Coloque el número del rack',
					related='contenedor.posicion_trasera'
		),
		'active':fields.boolean(
					'Activo',
					help='Si esta activo el motor lo incluira'
		),
	}
	_defaults={
		'active':True,
	}