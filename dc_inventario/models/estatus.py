# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class estatus(osv.osv):
	_name='reg.estatus'
	_rec_name='nombre'
	
	_columns={
		'nombre':fields.char('Estatus',size=80,required=False,help='Estatus del equipo'),
		'active':fields.boolean('Activo',help='Si esta activo el motor lo inclurá')
	}
	_defaults={
		'active':True,
	
	}
