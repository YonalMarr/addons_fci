# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class marca(osv.osv):
	_name='reg.gestor'
	_rec_name='nombre'
	
	_columns={
		'nombre':fields.char('Nombre',size=80,required=False,help='Nombre gestor de la base de datos'),
	}
