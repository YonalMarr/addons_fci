# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
import time

class servidores(osv.osv):
	_name='reg.servidores'
	_rec_name='nombre'
	
	_columns= {
		'imagen': fields.binary(
				"Referencia",
			help="This field holds the image used as image for our customers, limited to 1024x1024px."
		),
		'nombre':fields.char(
					'Nombre del Equipo',
					size=80,
					required=True,
					help='Aqui se coloca el Nombre del Equipo',
		),
		'tipo':fields.selection(
					[('fisico','Físico'),('virtual','Virtual'),('desktop','Desktop'),('desktopc','Desktop PC')], 'Tipo',
		),
		'interface_ids':fields.one2many(
					'reg.interface',
					'servidor_id',
					'Interface',
					required=True,
		),
		'servicio_ids':fields.one2many(
					'reg.servicio',
					'servidor_id',
					'Servicios',
					required=True,
		),
		'marca_id':fields.many2one(
					'reg.marca',
					'Marca',
					required=False,
		),
		'modelo_id':fields.many2one(
					'reg.modelo',
					'Modelo',
					required=False,
		),
		'serial':fields.char(
					'Serial',
					size=30,
					required=True,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'sistema_op_id':fields.selection(
					[('debian','Debian Server 64 bits'),('debian2','Debian Server consola 64 bits'),('debian3','Debian Server gráfico 64 bits'),('debian4','Debian Server 32 bits'),('windows','Windows Server 2003'),('windows2','Windows Server 2012')], 'Sistema Operativo'
		),
		'procesador':fields.char(
					'Procesador',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'memoria':fields.char(
					'Memoria',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'disco':fields.char(
					'Disco',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),	
		'usuario':fields.char(
					'Usuario',
					size=40,
					required=True,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'clave':fields.char(
					'Clave',
					size=40,
					required=True,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'estatus_id':fields.many2one(
					'reg.estatus',
					'Estatus',
					required=True,
		),
		'ssh':fields.selection(
					[('Si','Si'),('No','No')],'ssh'
		),
		'puerto':fields.char(
					'Puerto',
					required=False,
					size=6,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'rack':fields.char(
					'Rack',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'posicion_frontal':fields.char(
					'Posición Frontal',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'posicion_trasera':fields.char(
					'Posición Trasera',
					size=40,
					required=False,
					help='Aqui se coloca el numero de parte del repuesto',
		),
		'active':fields.boolean(
					'Activo',
					help='Si esta activo el motor lo incluira'
		),
	}
	_defaults={
		'active':True,
	}
