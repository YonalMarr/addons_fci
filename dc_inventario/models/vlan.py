# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class vlan(osv.osv):
	_name='reg.vlan'
	_rec_name='vlan'
	
	_columns={
		'vlan':fields.char('Vlan',size=80,required=False,help='Nombre de la cateogria'),
		'gerencia':fields.char('Gerencia',size=80,required=False,help='Nombre de la Gerencia a la que pertenece'),
		'puertos':fields.char('Puertos', size=30),
		'vlan_id':fields.many2one('reg.swicth', 'Swicth'),
		'active':fields.boolean('Activo',help='Si esta activo se inclurá'),
	}
	_defaults={
		'active':True,
	
	}
