# -*- coding: utf-8 -*-

import json
import logging
import base64
from cStringIO import StringIO

import openerplib

import openerp.exceptions
from werkzeug.exceptions import HTTPException
from openerp import http,tools, api,SUPERUSER_ID
from openerp.http import request
from openerp.addons.website_apiform.controladores import panel, base_tools
from datetime import datetime, date, time, timedelta
import hashlib
import random

import sys
reload(sys)
sys.setdefaultencoding('utf-8')
key = 'abcdefghijklmnopqrstuvwxyz0123456789'

class evaluacioness(http.Controller):

    def instanciar_objetos(self,objeto,parametro):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        objeto = registry.get(objeto)
        ids = objeto.search(cr,SUPERUSER_ID,parametro,context=context)
        data = objeto.browse(cr,SUPERUSER_ID,ids,context=context)
        return data


    def encrypt(self,plaintext):
        """Encrypt the string and return the ciphertext"""
        result = ''

        for l in plaintext.lower():
            try:
                i = (key.index(l) + 5) % 36
                result += key[i]
            except ValueError:
                result += l

        return result.lower()


    def decrypt(self,ciphertext):
        """Decrypt the string and return the plaintext"""
        result = ''

        for l in ciphertext:
            try:
                i = (key.index(l) - 5) % 36
                result += key[i]
            except ValueError:
                result += l

        return result

    @http.route('/evaluaciones/actualizar', auth='user', website=True)
    def actualizacion(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        eva_data=self.instanciar_objetos('fci_gh.eva_empleados',[])
        maximo = len(eva_data)
        i = 0
        while i < maximo:
            nombre = eva_data[i].empleado_id.name_related.split(' ')
            id_clave = self.encrypt(nombre[0] + nombre[1] + str(eva_data[i].id))
            eva = registry.get('fci_gh.eva_empleados')
            value={
            'id_clave':id_clave
            }
            clave = eva.write(cr, SUPERUSER_ID, eva_data[i].id, value, context=context)
            i += 1
        datos={ 'mensaje':'Listo', }
        return json.dumps(datos, sort_keys=True)


#################################################################################################
######################################EVALUACIONES###############################################
#################################################################################################
    @http.route('/evaluaciones', auth='user', website=True)
    def eva_principal(self):
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        employee_obj = registry.get("hr.employee")
        obj3=self.instanciar_objetos('hr.employee',[('user_id','=',uid)])
        obj4=self.instanciar_objetos('res.users',[('id','=',uid)])
        obj5=self.instanciar_objetos('hr.employee',[('name_related','like',obj4.partner_id.name.split(' ')[0].upper())])

        print obj3
        print obj4
        print obj5

        if uid == 5:
            employee_ids = employee_obj.search(cr,SUPERUSER_ID,[('coach_id','=',obj5.id)],context=context)
        elif uid == 25:
            employee_ids = employee_obj.search(cr,SUPERUSER_ID,[('parent_id','=',obj3.id)],context=context)
        else:
            employee_ids = employee_obj.search(cr,SUPERUSER_ID,[('coach_id','=',obj3.id),('id','!=',1)],context=context)
        #employee_ids = employee_obj.search(cr,SUPERUSER_ID,[('coach_id','=',obj3.id)],context=context)
        print employee_ids
        data_employee = employee_obj.read(cr,uid,employee_ids,['name_related','department_id'],context=context)
        maximo = len(data_employee)
        i = 0
        while i < maximo:
            id_eva = data_employee[i]['id']

            name = data_employee[i]['name_related'].replace(' ','')
            #name = name[0] + name[1]

            ciclo_obj = registry.get("fci_gh.configfecha")
            ciclo_id = ciclo_obj.search(cr,SUPERUSER_ID,[('activo','=',True)],context=context)

            eva_obj = registry.get("fci_gh.eva_empleados")
            eva_id = eva_obj.search(cr,uid,[('empleado_id','=',id_eva),('configfecha_id','in',ciclo_id)],context=context)
            eva_data = eva_obj.browse(cr, uid, eva_id, context)

            if eva_data:
                dept=self.instanciar_objetos('hr.department',[('id','=',data_employee[i]['department_id'][0])])

                data_employee[i]['department'] = dept.name
                if eva_data.id_clave == False:
                    data_employee[i]['id_clave'] = self.encrypt(name+str(date.today().timetuple().tm_yday))
                else:
                    data_employee[i]['id_clave'] = eva_data.id_clave

                data_employee[i]['actuacion'] = 'Sin Evaluar'
                if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
                    if eva_data.actuacion == 'Desempeño Excepcional':
                        actuaciones = eva_data.actuacion
                    else:
                        actuaciones = eva_data.actuacion.split('/')[0]
                else:
                    if eva_data.actuacion == 'Desempeño Excelente' or eva_data.actuacion == 'Desempeño Excepcional':
                        actuaciones = eva_data.actuacion
                    else:
                        actuaciones = eva_data.actuacion.split('/')[1]
                if eva_data['state'] == 'inicio':
                    if eva_data.total_eva == 0.0:
                        data_employee[i]['actuacion'] = 'Sin Evaluar'
                        data_employee[i]['state'] = 'Evaluacion Iniciada'
                    else:
                        data_employee[i]['state'] = 'Evaluacion Iniciada'
                        data_employee[i]['actuacion'] = actuaciones

                elif eva_data['state'] == 'recibida':
                    data_employee[i]['state'] = 'Evaluacion Recibida'
                    data_employee[i]['actuacion'] = actuaciones
                elif eva_data['state'] == 'revisada':
                    data_employee[i]['state'] = 'Evaluacion Revisada'
                    data_employee[i]['actuacion'] = actuaciones
                elif eva_data['state'] == 'enviada':
                    data_employee[i]['state'] = 'Evaluacion Enviada'
                    data_employee[i]['actuacion'] = actuaciones
                else:
                    data_employee[i]['state'] = 'Evaluacion Validada'
                    data_employee[i]['actuacion'] = actuaciones

            else:
                dept=self.instanciar_objetos('hr.department',[('id','=',data_employee[i]['department_id'][0])])
                data_employee[i]['department'] = dept.name
                data_employee[i]['id_clave'] = self.encrypt(name+str(date.today().timetuple().tm_yday))
                data_employee[i]['state'] = 'Evaluacion Sin Iniciar'
                data_employee[i]['actuacion'] = 'Sin Evaluar'

            i += 1

        ciclo_data=self.instanciar_objetos('fci_gh.configfecha',[('activo','=',True)])
        if ciclo_data:
            fecha = 'activa'
        else:
            fecha = 'no activa'


        return http.request.render('evaluaciones.evaluaciones',{'datos':data_employee,'fecha':fecha})

    @http.route(['/evaluaciones/<id>'], type='http', auth="user", website=True)
    def evaluaciones_detalle(self,id):
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        eva_data=self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',id)])
        employee_data=self.instanciar_objetos('hr.employee',[('id','=',eva_data.empleado_id.id)])

        valores={}
        ciclo_data=self.instanciar_objetos('fci_gh.configfecha',[('activo','=',True)])

        if eva_data.id == False:
            clave = self.decrypt(id)
            employee=self.instanciar_objetos('hr.employee',[])
            maximo = len(employee)
            fi = 0
            while fi < maximo:
                name = employee[fi].name_related.replace(" ","")
                if (name+str(date.today().timetuple().tm_yday)) == clave.upper():
                    employee_i = employee[fi].id
                    break
                fi+=1

            employee_data=self.instanciar_objetos('hr.employee',[('id','=',employee_i)])
            cargo_data=self.instanciar_objetos('fci_gh.con_cargo',[('id','=',employee_data.cargo_id['id'])])
            if 'URE' in employee_data.department_id.name:
                ure = 'inicio'
            else:
                ure = 'revisado'

            clave_e = self.encrypt(id)
            valores={
            'tipo_evaluacion_id':cargo_data.tipoevaluacion_id['id'],
            'empleado_id':employee_data.id,
            'state':'inicio',
            'configfecha_id':ciclo_data.id,
            'tipo_evaluacion':cargo_data.tipoevaluacion_id['nombre_evaluacion'],
            'cedula_identidad':employee_data.identification_id,
            'dias_salario':30,
            'actuacion':'Sin Evaluar',
            'total_eva':0,
            'total_odi':0,
            'total_tec':0,
            'work_email':employee_data.work_email,
            'total_comp':0,
            'id_clave':clave_e,
            'f_ure':ure
            }
            eva_data=self.instanciar_objetos('fci_gh.eva_empleados',[('empleado_id','=',employee_data.id),('configfecha_id','=',ciclo_data.id)])
            eva_obj = registry.get("fci_gh.eva_empleados")

            if eva_data:
                valores = {'id_clave':clave_e,}
                insert_odi = eva_obj.write(cr, SUPERUSER_ID, eva_data.id, valores, context=context)
            else:
                insert_odi = eva_obj.create(cr, SUPERUSER_ID, valores, context=context)
                eva_data=self.instanciar_objetos('fci_gh.eva_empleados',[('empleado_id','=',employee_data.id),('configfecha_id','=',ciclo_data.id)])

        evaluaciones_line_obj = registry.get("fci_gh.eva_empleados.line")
        odi_line_id=evaluaciones_line_obj.search(cr,SUPERUSER_ID,[('eva_empleados_line_id','=',eva_data[0]['id'])],context=context)
        odi_data = evaluaciones_line_obj.read(cr,SUPERUSER_ID,odi_line_id,[
                                                'id',
                                                'actividades',
                                                'peso',
                                                'subtotal',
                                                'rangocualitativo_id'
                                                ],context=context)
        maximo = len(odi_data)
        i = 0
        while i < maximo:
            cont = i + 1
            rangoStr = odi_data[i]['rangocualitativo_id'][1].split('/')
            if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
                odi_data[i]['rango'] = rangoStr[1]
            else:
                odi_data[i]['rango'] = rangoStr[0]
            odi_data[i]['rango_id'] = odi_data[i]['rangocualitativo_id'][0]
            i += 1

        if eva_data.tipo_evaluacion=='EMPLEADO / CONTRATADOS':
            comp_line_id=evaluaciones_line_obj.search(cr,SUPERUSER_ID,[('eva_empleados_line_id1','=',eva_data[0]['id'])],context=context)
        else:
            comp_line_id=evaluaciones_line_obj.search(cr,SUPERUSER_ID,[('eva_empleados_line_id2','=',eva_data[0]['id'])],context=context)

        comp_data = evaluaciones_line_obj.read(cr,SUPERUSER_ID,comp_line_id,[
                                                'id',
                                                'competencia_id',
                                                'peso',
                                                'subtotal',
                                                'rangocualitativo_id'
                                                ],context=context)

        if eva_data.tipo_evaluacion=='EMPLEADO / CONTRATADOS':
            compet_d = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id1','=',int(eva_data[0]['id']))])
        else:
            compet_d = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id2','=',int(eva_data[0]['id']))])

        maximo = len(comp_data)
        c = 0
        while c < maximo:
            cont = c + 1
            rangoStr = compet_d[c].rangocualitativo_id.nombre_rango_cualitativo.split('/')
            if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
                comp_data[c]['rango'] = rangoStr[1]
            else:
                comp_data[c]['rango'] = rangoStr[0]
            #comp_data[c]['rango'] = compet_d[c].rangocualitativo_id.nombre_rango_cualitativo
            comp_data[c]['rango_id'] = compet_d[c].rangocualitativo_id.id
            comp_data[c]['competencia'] =  compet_d[c].competencia_id.descripcion
            comp_data[c]['comp_id'] =  compet_d[c].competencia_id.id
            c += 1

        rango_obj = registry.get("fci_gh.rangocualitativo")
        rango_obj_id=rango_obj.search(cr,SUPERUSER_ID,[],context=context)
        rango_obj_data = rango_obj.read(cr,SUPERUSER_ID,rango_obj_id,[
                                                'id',
                                                'nombre_rango_cualitativo',
                                                'valor_rango',
                                                'valor_rango_tecnico'
                                                ],context=context)
        rango_data=self.instanciar_objetos('fci_gh.rangocualitativo',[])
        r = 0
        while r < len(rango_obj_data):
            if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
                rango_obj_data[r]['nombre']=rango_obj_data[r]['nombre_rango_cualitativo'].split("/")[1]
            else:
                rango_obj_data[r]['nombre']=rango_obj_data[r]['nombre_rango_cualitativo'].split("/")[0]
            #rangoStr_data[r]['id']=rango_data[r].id
            r += 1
        comp_select_data=self.instanciar_objetos('fci_gh.con_competencia',[])

        crit_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id3','=',int(eva_data[0]['id']))])
        logro_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id4','=',int(eva_data[0]['id']))])
        imp_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id5','=',int(eva_data[0]['id']))])
        inst_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id6','=',int(eva_data[0]['id']))])

        date_write_conv = datetime.strptime(eva_data.write_date, '%Y-%m-%d %H:%M:%S' )
        post_date = date_write_conv + timedelta(days=1)
        if (datetime.now() > post_date) and (eva_data.state == 'recibida'):
            value={'state':'revisada'}
            eva_emple = registry.get('fci_gh.eva_empleados')
            act_evaemp = eva_emple.write(cr, uid, eva_data.id, value, context=context)

        if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
            if eva_data['actuacion'] == 'Desempeño Excepcional':
                actuacion_spl = eva_data['actuacion']
            else:
                actuacion_spl = eva_data['actuacion'].split('/')[0]
        else:
            if eva_data['actuacion'] == 'Desempeño Excelente' or eva_data['actuacion'] == 'Desempeño Excepcional':
                actuacion_spl = eva_data['actuacion']
            else:
                actuacion_spl = eva_data['actuacion'].split('/')[1]

        if eva_data.total_eva == 0.0:
            actuacion =''
        else:
            actuacion =  actuacion_spl

        if 'URE' in employee_data.department_id.name:
            if uid == 25:
                if_ure = 'NO'
            else:
                if_ure = 'SI'
        else:
            if_ure = 'NO'

        return http.request.render('evaluaciones.detalle_personal',
            {
                        'if_ure':if_ure,
                        'eva_data':eva_data,
                        'employee_data':employee_data,
                        'actuacion':actuacion,
                        'ciclo_data':ciclo_data,
                        'odi_data':odi_data,
                        'rango_data':rango_obj_data,
                        'comp_select_data':comp_select_data,
                        'comp_data':comp_data,
                        'crit_data':crit_data,
                        'logro_data':logro_data,
                        'imp_data':imp_data,
                        'inst_data':inst_data,
                        })

    @http.route(['/evaluaciones/crear/odi/guardar'], type='http', auth="user", website=True)
    def accion_guardar_odi(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos={}
        res_users_obj = registry.get('fci_gh.eva_empleados.line')
        ir_action_obj = registry.get('ir.actions.actions')
        eva_emple = registry.get('fci_gh.eva_empleados')
        rango_data = self.instanciar_objetos('fci_gh.rangocualitativo',[('valor_rango','=',post['rango']),])
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])
        odi_line =self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id','=',eva_data['id']),('id','!=',int(post['id_line']))])
        maximo = len(odi_line)
        i = 0
        total_odi = 0
        total_peso = 0

        while i < maximo:
            total_odi = total_odi + odi_line[i].subtotal
            total_peso = total_peso + odi_line[i].peso
            i += 1
        total_odi = total_odi + float(post['subtotal'])
        total_peso = total_peso + float(post['peso'])

        if total_odi > 250 or total_peso > 50:
            datos={'mensaje':True,}
            return json.dumps(datos, sort_keys=True)
        else:
            value = {
            'actividades':post['act'],
            'rangocualitativo_id':rango_data['id'],
            'peso':post['peso'],
            'eva_empleados_line_id':eva_data['id'],
            'subtotal':post['subtotal'],
            }

            value_2 = {
            'total_odi':total_odi
            }
            if post['id_line'] == '0':
                user_id = res_users_obj.create(cr, uid, value, context=context)
                act_evaemp = eva_emple.write(cr, uid, eva_data.id, value_2, context=context)
            else:
                line = registry.get('fci_gh.eva_empleados.line')
                line_id =line.search(cr,uid,[('id','=',post['id_line'])],context=context)
                user_id = res_users_obj.write(cr, uid, line_id, value, context=context)
                act_evaemp = eva_emple.write(cr, uid, eva_data.id, value_2, context=context)

            rec_obj=registry.get('fci_gh.rangoactuacion')
            if eva_data.tipo_evaluacion != 'EMPLEADO / CONTRATADOS':
                resultado = rec_obj.search(cr, uid, [], order='rango_tecnico', context=context)
            else:
                resultado = rec_obj.search(cr, uid, [], order='rango', context=context)
            a = 0
            valor_evaluar = eva_data.total_eva
            escala_final = False
            for i in resultado:
                for resp in rec_obj.browse(cr,uid,i):
                    if eva_data.tipo_evaluacion == 'EMPLEADO / CONTRATADOS':
                        valor_rango = resp.rango
                    else:
                        valor_rango = resp.rango_tecnico
                    if valor_evaluar <= valor_rango:
                        escala_final = resp.escala_cualitativa.encode('utf8')
                        break
                if escala_final:
                    break
            value_3 = {
            'actuacion':total_odi
            }
            act_evaemp = eva_emple.write(cr, uid, eva_data.id, value_3, context=context)
            if user_id:
                line = post['id_line']

            if user_id != True:
                line = user_id
            if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
                rango = rango_data.nombre_rango_cualitativo.split("/")[1]
                escala_final = escala_final.split('/')[0]
            else:
                rango = rango_data.nombre_rango_cualitativo.split("/")[0]
                escala_final = escala_final.split('/')[1]


            datos={ 'mensaje':False,
                    'actuacion':escala_final,
                    'total_odi':total_odi,
                    'total_eva':eva_data.total_eva,
                    'id_line':line,
                    'rango':rango
                    }
            return json.dumps(datos, sort_keys=True)


    @http.route(['/evaluaciones/crear/odi/eliminar'],type='http', auth="user", website=True)
    def accion_eliminar_odi(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos = {}
        total_comp = 0
        total_odi = 0
        total_tec = 0
        total_eva = 0
        line_id = self.instanciar_objetos('fci_gh.eva_empleados.line',[('id','=',int(post['id_line']))])
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])

        if line_id.eva_empleados_line_id1:
            total_comp = eva_data.total_comp - line_id.subtotal
            total_eva = eva_data.total_eva - line_id.subtotal
            value={
            'total_comp':total_comp,
            'total_eva':total_eva
            }
        elif line_id.eva_empleados_line_id2:
            total_tec = eva_data.total_tec - line_id.subtotal
            total_eva = eva_data.total_eva - line_id.subtotal
            value={
            'total_tec':total_tec,
            'total_eva':total_eva

            }
        elif line_id.eva_empleados_line_id:
            total_odi = eva_data.total_odi - line_id.subtotal
            total_eva = eva_data.total_eva - line_id.subtotal
            value={
            'total_odi':total_odi,
            'total_eva':total_eva
            }
        line = registry.get('fci_gh.eva_empleados.line')
        line_data = line.unlink(cr, uid, line_id.id, context=None)
        if line_data:
            rec_obj=registry.get('fci_gh.rangoactuacion')
            if eva_data.tipo_evaluacion != 'EMPLEADO / CONTRATADOS':
                resultado = rec_obj.search(cr, uid, [], order='rango_tecnico', context=context)
            else:
                resultado = rec_obj.search(cr, uid, [], order='rango', context=context)
            a = 0
            valor_evaluar = total_eva
            escala_final = False
            for i in resultado:
                for resp in rec_obj.browse(cr,uid,i):
                    if eva_data.tipo_evaluacion == 'EMPLEADO / CONTRATADOS':
                        valor_rango = resp.rango
                    else:
                        valor_rango = resp.rango_tecnico
                    if valor_evaluar <= valor_rango:
                        escala_final = resp.escala_cualitativa.encode('utf8')
                        break
                if escala_final:
                    break
            value['actuacion'] = escala_final
            eva_emple = registry.get('fci_gh.eva_empleados')
            act_evaemp = eva_emple.write(cr, uid, eva_data.id, value, context=context)

            eva_data_final = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])


            if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
                if eva_data_final.total_eva == 0:
                    escala_final = ''
                else:
                    escala_final = eva_data_final.actuacion.split('/')[0]
            else:
                if eva_data_final.total_eva == 0:
                    escala_final = ''
                else:
                    escala_final = eva_data_final.actuacion.split('/')[1]

            datos = {'valor': True,
                    'total_eva':eva_data_final.total_eva,
                    'total_odi':eva_data_final.total_odi,
                    'total_comp':eva_data_final.total_comp,
                    'total_tec':eva_data_final.total_tec,
                    'actuacion':escala_final,
                }
            return json.dumps(datos, sort_keys=True)

    @http.route(['/evaluaciones/contar_odi'],type='http', auth="user", website=True)
    def contar_odi(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos = {}

        #eva = registry.get('fci_gh.eva_empleados')
        #eva_id = eva.search(cr,uid,[('id_clave','=',post['empleado'])],context=context)
        #eva_Data = eva.read(cr,uid,eva_id,['id',],context=context)

        eva_Data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])
        cargo = eva_Data.empleado_id.cargo_id.cargo
        odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id','=',eva_Data.id)])

        rango_data = self.instanciar_objetos('fci_gh.rangocualitativo',[])
        maximo = len(rango_data)
        i = 0
        rango=[]
        valor=[]
        while i < maximo:
            rango.append(rango_data[i].nombre_rango_cualitativo)
            valor.append(rango_data[i].valor_rango)
            i += 1

        X = len(odi_data)
        datos = {'cargo':cargo,'numero': X,'max_rango':maximo,'rango':rango,'valor':valor}
        return json.dumps(datos, sort_keys=True)


    @http.route(['/evaluaciones/contar_comp'],type='http', auth="user", website=True)
    def contar_comp(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos = {}

        eva = registry.get('fci_gh.eva_empleados')
        eva_id =eva.search(cr,uid,[('id_clave','=',post['empleado'])],context=context)
        eva_Data = eva.read(cr,uid,eva_id,[
                                                'id',
                                                ],context=context)
        if post['tipoevaluacion']=='EMPLEADO / CONTRATADOS':
            odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id1','=',eva_Data[0]['id'])])
        else:
            odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id2','=',eva_Data[0]['id'])])

        c=0
        arr=[]
        while c < len(odi_data):
            arr.append(odi_data[c].competencia_id['id'])
            c+=1


        rango_data = self.instanciar_objetos('fci_gh.rangocualitativo',[])
        tipoevaluacion = self.instanciar_objetos('fci_gh.con_tipoevalucion',[('nombre_evaluacion','=',post['tipoevaluacion'])])

        comp_select_data=self.instanciar_objetos('fci_gh.con_competencia',[('id','not in',arr),('tipo_evaluacion_id','=',tipoevaluacion.id)])
        competencia=[]
        id_comp=[]
        maximo_c = len(comp_select_data)
        c=0
        while c < maximo_c:
            competencia.append(comp_select_data[c].descripcion)
            id_comp.append(comp_select_data[c].id)
            c += 1

        maximo = len(rango_data)
        i = 0
        rango=[]
        valor=[]
        while i < maximo:
            rango.append(rango_data[i].nombre_rango_cualitativo)
            if post['tipoevaluacion']=='EMPLEADO / CONTRATADOS':
                valor.append(rango_data[i].valor_rango)
            else:
                valor.append(rango_data[i].valor_rango_tecnico)
            i += 1

        X = len(odi_data)
        datos = {'numero': X,
        'max_rango':maximo,
        'max_competencia':maximo_c,
        'rango':rango,
        'valor':valor,
        'competencia':competencia,
        'id_comp':id_comp}

        return json.dumps(datos, sort_keys=True)

    @http.route(['/evaluaciones/crear/comp/guardar'], type='http', auth="user", website=True)
    def accion_guardar_comp(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos={}
        selc = ''
        res_users_obj = registry.get('fci_gh.eva_empleados.line')
        ir_action_obj = registry.get('ir.actions.actions')
        eva_emple = registry.get('fci_gh.eva_empleados')
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])
        rango_act = self.instanciar_objetos('fci_gh.rangoactuacion',[])
        comp_sel = self.instanciar_objetos('fci_gh.con_competencia',[('id','=',int(post['act']))])

        if post['tipoevaluacion']=='EMPLEADO / CONTRATADOS':
            rango_data = self.instanciar_objetos('fci_gh.rangocualitativo',[('valor_rango','=',post['rango']),])
            comp_line =self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id1','=',eva_data['id']),('id','!=',int(post['id_line']))])
        else:
            rango_data = self.instanciar_objetos('fci_gh.rangocualitativo',[('valor_rango_tecnico','=',post['rango']),])
            comp_line =self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id2','=',eva_data['id']),('id','!=',int(post['id_line']))])

        maximo = len(comp_line)
        i = 0
        total_comp = 0
        total_peso = 0

        while i < maximo:
            if post['tipoevaluacion']=='EMPLEADO / CONTRATADOS':
                total_comp = total_comp + comp_line[i].subtotal
                total_peso = total_peso + comp_line[i].peso
            else:
                total_comp = total_comp + comp_line[i].subtotal
            i += 1
        if post['tipoevaluacion']=='EMPLEADO / CONTRATADOS':
            total_comp = total_comp + float(post['subtotal'])
            total_peso = total_peso + float(post['peso'])
        else:
            total_comp = total_comp + float(post['subtotal'])


        if total_comp > 250 or total_peso > 50:
            datos={'mensaje':True,}
            return json.dumps(datos, sort_keys=True)
        else:
            if post['tipoevaluacion']=='EMPLEADO / CONTRATADOS':
                value = {
                'competencia_id':int(post['act']),
                'rangocualitativo_id':rango_data['id'],
                'peso':post['peso'],
                'eva_empleados_line_id1':eva_data['id'],
                'subtotal':post['subtotal'],
                }

                value_2 = {
                'total_comp':total_comp
                }
            else:
                value = {
                'competencia_id':int(post['act']),
                'rangocualitativo_id':rango_data['id'],
                'eva_empleados_line_id2':eva_data['id'],
                'subtotal':post['subtotal']
                }

                value_2 = {
                'total_tec':total_comp
                }
            if post['id_line'] == '0':
                user_id = res_users_obj.create(cr, uid, value, context=context)
                act_evaemp = eva_emple.write(cr, uid, eva_data.id, value_2, context=context)
            else:
                line = registry.get('fci_gh.eva_empleados.line')
                line_id =line.search(cr,uid,[('id','=',post['id_line'])],context=context)
                user_id = res_users_obj.write(cr, uid, line_id, value, context=context)
                act_evaemp = eva_emple.write(cr, uid, eva_data.id, value_2, context=context)


            rec_obj=registry.get('fci_gh.rangoactuacion')
            if eva_data.tipo_evaluacion != 'EMPLEADO / CONTRATADOS':
                resultado = rec_obj.search(cr, uid, [], order='rango_tecnico', context=context)
            else:
                resultado = rec_obj.search(cr, uid, [], order='rango', context=context)
            a = 0
            valor_evaluar = eva_data.total_eva
            escala_final = False
            for i in resultado:
                for resp in rec_obj.browse(cr,uid,i):
                    if eva_data.tipo_evaluacion == 'EMPLEADO / CONTRATADOS':
                        valor_rango = resp.rango
                    else:
                        valor_rango = resp.rango_tecnico
                    if valor_evaluar <= valor_rango:
                        escala_final = resp.escala_cualitativa.encode('utf8')
                        break
                if escala_final:
                    break
            value_3 = {
            'actuacion':escala_final
            }
            act_evaemp = eva_emple.write(cr, uid, eva_data.id, value_3, context=context)
            if user_id:
                line = post['id_line']

            if user_id != True:
                line = user_id
            selc = comp_sel.descripcion
            if eva_data.empleado_id.cargo_id.cargo == 'ANALISTA':
                rango = rango_data.nombre_rango_cualitativo.split("/")[1]
                if eva_data.actuacion == 'Desempeño Excepcional':
                    escala_final = eva_data.actuacion
                else:
                    escala_final = eva_data.actuacion.split('/')[0]
            else:
                rango = rango_data.nombre_rango_cualitativo.split("/")[0]
                if eva_data.actuacion == 'Desempeño Excelente':
                    escala_final = eva_data.actuacion
                else:
                    escala_final = eva_data.actuacion.split('/')[1]



            datos={ 'mensaje':False,
                    'actuacion':escala_final,
                    'total_comp':total_comp,
                    'total_tec':total_comp,
                    'total_eva':eva_data.total_eva,
                    'selc':selc,
                    'id_line':line,
                    'rango':rango,
                    }
            return json.dumps(datos, sort_keys=True)




    @http.route(['/evaluaciones/contar_exc'],type='http', auth="user", website=True)
    def contar_exc(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos = {}

        eva = registry.get('fci_gh.eva_empleados')
        eva_id =eva.search(cr,uid,[('id_clave','=',post['empleado'])],context=context)

        eva_Data = eva.read(cr,uid,eva_id,['id',],context=context)
        if post['tipo']=='criterio':
            odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id3','=',eva_Data[0]['id'])])
        elif post['tipo']=='logro':
            odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id4','=',eva_Data[0]['id'])])
        elif post['tipo']=='impacto':
            odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id5','=',eva_Data[0]['id'])])
        else:
            odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id6','=',eva_Data[0]['id'])])

        X = len(odi_data)
        datos = {'numero': X,}
        return json.dumps(datos, sort_keys=True)

    @http.route(['/evaluaciones/crear/excelente/guardar'],type='http', auth="user", website=True)
    def guardar_exc(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos = {}
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])
        res_users_obj = registry.get('fci_gh.eva_empleados.line')

        if post['tipo']=='criterio':
            value = {
            'actividades1':post['act'],
            'eva_empleados_line_id3':eva_data['id'],
            }
        elif post['tipo']=='logro':
            value = {
            'actividades2':post['act'],
            'eva_empleados_line_id4':eva_data['id'],
            }
        elif post['tipo']=='impacto':
            value = {
            'actividades3':post['act'],
            'eva_empleados_line_id5':eva_data['id'],
            }
        else:
            value = {
            'actividades4':post['act'],
            'eva_empleados_line_id6':eva_data['id'],
            }
        if post['id_line'] == '0':
            user_id = res_users_obj.create(cr, uid, value, context=context)
            if user_id:
                print 'SI GUARDO'
            else:
                print 'NO GUARDO'
        else:
            line = registry.get('fci_gh.eva_empleados.line')
            line_id =line.search(cr,uid,[('id','=',post['id_line'])],context=context)
            user_id = res_users_obj.write(cr, uid, line_id, value, context=context)
            print 'LO EDITO'
        if user_id:
            line = post['id_line']
        if user_id != True:
            line = user_id
        datos={'mensaje':False,'id_line':line}

        return json.dumps(datos, sort_keys=True)

    @http.route(['/evaluaciones/crear/exec/eliminar'],type='http', auth="user", website=True)
    def eliminar_exec(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        datos = {}
        line_id = self.instanciar_objetos('fci_gh.eva_empleados.line',[('id','=',int(post['id_line']))])
        line = registry.get('fci_gh.eva_empleados.line')
        line_data = line.unlink(cr, uid, line_id.id, context=None)
        if line_data:
            datos = {'valor': True}
            return json.dumps(datos, sort_keys=True)


    @http.route(['/revision/analista/<id>'],type='http', auth="public", website=True)
    def revision_analista(self,id):
        empleado = id
        return self.evaluaciones_detalle(empleado)



    @http.route(['/evaluaciones/revisar'],type='http', auth="public", website=True)
    def enviar_evaluador(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])

        if post['observacion'] == '':
            datos = {'valor': False}
            return json.dumps(datos, sort_keys=True)
        else:
            value={
                'state':'revisada',
               'observ_odi':post['observacion']
                }
            line = registry.get('fci_gh.eva_empleados')
            line_id =line.search(cr,SUPERUSER_ID,[('id','=',eva_data.id)],context=context)
            user_id = line.write(cr, SUPERUSER_ID, line_id, value, context=context)
            datos = {'valor': True}
            return json.dumps(datos, sort_keys=True)

    @http.route(['/evaluaciones/rrhh'],type='http', auth="user", website=True)
    def enviar_rrhh(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])

        #if post['observacion'] == '':
        #    datos = {'valor': False}
        #    return json.dumps(datos, sort_keys=True)
        #else:
        value={
            'state':'enviada',
            }
        line = registry.get('fci_gh.eva_empleados')
        line_id =line.search(cr,SUPERUSER_ID,[('id','=',eva_data.id)],context=context)
        user_id = line.write(cr, SUPERUSER_ID, line_id, value, context=context)
        datos = {'valor': True}
        return json.dumps(datos, sort_keys=True)


    @http.route(['/evaluaciones/ure/'],type="http",auth="user",website=True)
    def enviar_ure(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        suma2 = 0.00
        mensaje = 'NO'

        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])
        if post['tipoevaluacion'] =='EMPLEADO / CONTRATADOS':
            comp_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id1','=',eva_data.id)])
        else:
            comp_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id2','=',eva_data.id)])

        if post['tipoevaluacion'] =='EMPLEADO / CONTRATADOS':

            maximo_cm = len(comp_data)
            f = 0
            while f < maximo_cm:
                suma2 = suma2 + comp_data[f].peso
                f += 1
            cant_comp = 8
        else:
            maximo_cm = len(comp_data)
            cant_comp = 0
            while cant_comp < maximo_cm:
                cant_comp += 1
            suma2 = 50
        if suma2 != 50 or cant_comp != 8 or f != 6:
            mensaje = 'SI'

        if mensaje == 'NO':
            value={
            'f_ure':'revisado',
            }

            line = registry.get('fci_gh.eva_empleados')
            line_id =line.search(cr,uid,[('id','=',eva_data.id)],context=context)
            user_id = line.write(cr, uid, line_id, value, context=context)

            if user_id:
                datos = {'mensaje': mensaje}
                return json.dumps(datos, sort_keys=True)
        else:
            datos = {'mensaje': mensaje}
            return json.dumps(datos, sort_keys=True)


    @http.route(['/evaluaciones/enviar'],type='http', auth="user", website=True)
    def enviar_analista(self,**post):
        suma = 0.00
        suma2 = 0.00
        mensaje = 'NO'
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])
        odi_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id','=',eva_data.id)])
        if post['tipoevaluacion'] =='EMPLEADO / CONTRATADOS':
            comp_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id1','=',eva_data.id)])
        else:
            comp_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id2','=',eva_data.id)])

        crit_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id3','=',eva_data.id)])
        logro_data= self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id4','=',eva_data.id)])
        impac_data= self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id5','=',eva_data.id)])
        instl_data= self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id6','=',eva_data.id)])

        if post['tipoevaluacion'] =='EMPLEADO / CONTRATADOS':
            maximo_od = len(odi_data)
            i = 0
            while i < maximo_od:
                suma = suma + odi_data[i].peso
                i+=1

            maximo_cm = len(comp_data)
            f = 0
            while f < maximo_cm:
                suma2 = suma2 + comp_data[f].peso
                f += 1
            cant_comp = 8
        else:
            maximo_cm = len(comp_data)
            cant_comp = 0
            while cant_comp < maximo_cm:
                cant_comp += 1
            suma = 50
            suma2 = 50
            i = 3
            f = 6
        if suma != 50 or suma2 != 50 or cant_comp != 8 or f != 6 or i < 3:
            mensaje = 'SI'

        if post['actuacion'] == 'Desempeño Excepcional':
            if len(crit_data)<3 or len(logro_data)<3 or len(impac_data)<3 or len(instl_data)<3:
                mensaje = 'SI'

        if mensaje == 'NO':
            value={
            'state':'recibida',
            }
            mail_template='email_template_analista'
            ir_model_data_obj = registry.get('ir.model.data')
            eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])

            template_id = ir_model_data_obj.get_object_reference(
                                            cr,
                                            uid,
                                            'evaluaciones',
                                            mail_template)[1]
            ctx = dict()
            ctx.update({

                'default_composition_mode': 'email',
                'default_subject': '',
                'mark_so_as_sent': True,
                'actuacion':'bIen',
            })
            value={
                'state':'recibida',
                #'recomen_odi':post['recomendacion']
                }
            line = registry.get('fci_gh.eva_empleados')
            line_id =line.search(cr,uid,[('id','=',eva_data.id)],context=context)
            user_id = line.write(cr, uid, line_id, value, context=context)

            registry.get('email.template').send_mail(cr, uid, template_id, eva_data.id, True, context=ctx)
            if user_id:
                datos = {'mensaje': mensaje}
                return json.dumps(datos, sort_keys=True)
        else:
            datos = {'mensaje': mensaje}
            return json.dumps(datos, sort_keys=True)


    @http.route(['/evaluaciones/recomendacion'],type='http', auth="user", website=True)
    def save_recomendacion(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])
        value = {
            'recomen_odi':post['recomendacion']
        }
        line = registry.get('fci_gh.eva_empleados')
        line_id =line.search(cr,uid,[('id','=',eva_data.id)],context=context)
        user_id = line.write(cr, uid, line_id, value, context=context)
        datos = {'mensaje': 'SI'}
        return json.dumps(datos, sort_keys=True)
###############################################################################
###############################################################################
###########################REVISION############################################
###############################################################################
###############################################################################

    @http.route('/revision', auth='user', website=True)
    def revision(self):
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('state','=','enviada')])
        actuacion = []
        maximo = len(eva_data)
        i = 0
        while i < maximo:
            if eva_data[i]['actuacion'] == 'Desempeño Excepcional' or eva_data[i]['actuacion'] == 'Desempeño Excelente':
                eva_data[i]['actuacion'] = eva_data[i]['actuacion']
            else:
                if eva_data[i]['empleado_id']['cargo_id']['cargo'] == 'ANALISTA':
                    eva_data[i]['actuacion'] = eva_data[i]['actuacion'].split('/')[0]
                else:
                    eva_data[i]['actuacion'] = eva_data[i]['actuacion'].split('/')[1]
            i += 1
        return http.request.render('evaluaciones.revision',{'eva_data':eva_data,})

    @http.route(['/evaluaciones/revision/<id>'], type='http', auth="user", website=True)
    def evaluaciones_revision(self,id):
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        id_empleado = id
        valores={}

        ciclo_data=self.instanciar_objetos('fci_gh.configfecha',[('activo','=',True)])

        eva_data=self.instanciar_objetos('fci_gh.eva_empleados',[('id','=',id),('configfecha_id','=',ciclo_data.id)])

        employee_data=self.instanciar_objetos('hr.employee',[('id','=',eva_data.empleado_id.id)])

        odi_data=self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id','=',eva_data.id)])

        if eva_data.tipo_evaluacion=='EMPLEADO / CONTRATADOS':
            comp_data=self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id1','=',eva_data.id)])
        else:
            comp_data=self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id2','=',eva_data.id)])

        rango_data=self.instanciar_objetos('fci_gh.rangocualitativo',[])
        comp_select_data=self.instanciar_objetos('fci_gh.con_competencia',[])

        crit_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id3','=',eva_data.id)])
        logro_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id4','=',eva_data.id)])
        imp_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id5','=',eva_data.id)])
        inst_data = self.instanciar_objetos('fci_gh.eva_empleados.line',[('eva_empleados_line_id6','=',eva_data.id)])


        return http.request.render('evaluaciones.nueva',            {
                        'eva_data':eva_data,
                        'employee_data':employee_data,
                        'ciclo_data':ciclo_data,
                        'odi_data':odi_data,
                        'rango_data':rango_data,
                        'comp_select_data':comp_select_data,
                        'comp_data':comp_data,
                        'crit_data':crit_data,
                        'logro_data':logro_data,
                        'imp_data':imp_data,
                        'inst_data':inst_data,
                        })

    @http.route(['/evaluaciones/regresar/'],type='http', auth="user", website=True)
    def regresar(self,**post):
        mensaje = 'NO'
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])

        if 'URE' in eva_data.empleado_id.department_id.name:
            ure = 'inicio'
        else:
            ure = 'revisado'
        value={ 'state':'inicio',
                'f_ure':ure}
        line = registry.get('fci_gh.eva_empleados')
        line_id =line.search(cr,uid,[('id','=',eva_data.id)],context=context)
        user_id = line.write(cr, uid, line_id, value, context=context)
        if user_id:
            datos = {'mensaje': mensaje}
            return json.dumps(datos, sort_keys=True)

    @http.route(['/evaluaciones/validar/'],type='http', auth="user", website=True)
    def validar(self,**post):
        mensaje = 'NO'
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        eva_data = self.instanciar_objetos('fci_gh.eva_empleados',[('id_clave','=',post['empleado'])])

        value={ 'state':'validada', }
        line = registry.get('fci_gh.eva_empleados')
        line_id =line.search(cr,uid,[('id','=',eva_data.id)],context=context)
        user_id = line.write(cr, uid, line_id, value, context=context)
        if user_id:
            mail_template='email_template_prueba'
            ir_model_data_obj = registry.get('ir.model.data')
            template_id = ir_model_data_obj.get_object_reference(
                                        cr,
                                        uid,
                                        'fci_gh_evaluacion',
                                        mail_template)[1]
            ctx = dict()
            ctx.update({

            'default_composition_mode': 'email',
            'default_subject': '',
            'mark_so_as_sent': True,
            'actuacion':'bIen',
            })
            registry.get('email.template').send_mail(cr, uid, template_id, eva_data.id, True, context=ctx)
            datos = {'mensaje': mensaje}
            return json.dumps(datos, sort_keys=True)


    @http.route(['/evaluacion/fecha_evaluacion'],type='http', auth="user", website=True)
    def fecha_evaluacion(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        fecha_data = self.instanciar_objetos('fci_gh.configfecha',[])
        now = datetime.now()
        fecha_act = self.instanciar_objetos('fci_gh.configfecha',[('activo','=',True)])
        if fecha_act:
            activo = 'si'
        else:
            activo = 'no'
        if now.month < 7 and now.month > 1:
            periodo = 'PRIMER'
        else:
            periodo = 'SEGUNDO'
        return http.request.render('evaluaciones.fecha_evaluacion',{'activo':activo,'fecha_data':fecha_data,'year':now.year,'periodo':periodo})

    @http.route(['/evaluaciones/activar_fecha'],type="http", auth="user", website=True)
    def activar_fecha(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        valores = {
        'ciclo_de_evaluacion':post['ciclo_de_evaluacion'],
        'fecha_inicio':post['fecha_inicio'],
        'fecha_fin':post['fecha_fin'],
        'activo':True
        }
        eva_obj = registry.get("fci_gh.configfecha")
        insert_odi = eva_obj.create(cr, SUPERUSER_ID, valores, context=context)

        datos = {'mensaje': 'si'}
        return json.dumps(datos, sort_keys=True)

    @http.route(['/evaluacion/fecha_evaluacion/reactivar'],type="http",auth="user",website=True)
    def reactivar_fecha(self,**post):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        valores = {
         'activo':True
        }
        fecha = registry.get("fci_gh.configfecha")
        fecha_id =fecha.search(cr,uid,[('id','=',int(post['id']))],context=context)
        fecha_data = fecha.write(cr, uid, fecha_id, valores, context=context)
        mensaje = 'si'

        return json.dumps(mensaje, sort_keys=True)

    @http.route(['/evaluacion/fecha_evaluacion/desac'],type="http",auth="user",website=True)
    def desactivar_fecha(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        now = datetime.now()

        valores = {
         'activo':False
        }
        fecha_act = self.instanciar_objetos('fci_gh.configfecha',[('activo','=',True)])
        if now > datetime.strptime(fecha_act.fecha_fin, '%Y-%m-%d'):
            fecha_obj = registry.get("fci_gh.configfecha")
            fecha_id =fecha_obj.search(cr,uid,[('activo','=',True)],context=context)
            user_id = fecha_obj.write(cr, uid, fecha_id, valores, context=context)
            mensaje = 'si'
        else:
            mensaje = 'no'

        return json.dumps(mensaje, sort_keys=True)

    @http.route('/evaluacion/departamentos', auth='user', website=True)
    def departamentos(self):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        dpto_data=self.instanciar_objetos("hr.department",[])
        return http.request.render('evaluaciones.departamentos',{'dpto':dpto_data})

    @http.route('/evaluacion/departametos/info/<id>', auth='user', website=True)
    def departamento_info(self,id):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        employee_data=self.instanciar_objetos("hr.employee",[('department_id','=',int(id)),('coach_id','!=',None)])
        dpto_data=self.instanciar_objetos("hr.department",[('id','=',id)])

        return http.request.render('evaluaciones.dpt_empleados',{'dpto':dpto_data,'employee':employee_data})
