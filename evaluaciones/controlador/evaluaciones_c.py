# -*- coding: utf-8 -*-

import json
import logging
import base64
from cStringIO import StringIO

import openerp.exceptions
from openerp.addons.web.controllers import main
from werkzeug.exceptions import HTTPException
from openerp import http,tools, api,SUPERUSER_ID
from openerp.http import request
from openerp.addons.website_apiform.controladores import panel, base_tools
from datetime import datetime, date, time, timedelta


_logger = logging.getLogger(__name__)

class evaluaciones_controlador(http.Controller):

    def instanciar_objetos(self,objeto,parametro):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        objeto = registry.get(objeto)
        ids = objeto.search(cr,SUPERUSER_ID,parametro,context=context)
        data = objeto.browse(cr,SUPERUSER_ID,ids,context=context)
        return data

    #@http.route('/evaluaciones', auth='user', website=True)
    #def evaluaciones(self):
    #    datos={'parametros':{
    #                'titulo':'Gestión de evaluaciones',
    #                'template':'evaluaciones.index',
    #                'url_boton_list':'',
    #                'css':'info',
    #                },
    #
    #                }
    #    return panel.panel_post(datos)

    @http.route('/principal', auth='user', website=True)
    def principal(self):

        return http.request.render('evaluaciones.principal')


#######################################################################
####################HISTORIAL##########################################
#######################################################################

    @http.route('/historial', auth='user', website=True)
    def historial_empleado(self):
        ret={}
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context

        evaluaciones_obj = registry.get("fci_gh.eva_empleados")
        conf_val_general_ids=evaluaciones_obj.search(cr,uid,[],context=context)

        ValGeneralData = evaluaciones_obj.read(cr,uid,conf_val_general_ids,[
                                                'cedula_identidad',
                                                'empleado_id',
                                                'configfecha_id',
                                                'state'

                                                ],context=context)

        maximo = len(ValGeneralData)
        i = 0
        while i < maximo:
            employeehr= self.instanciar_objetos('hr.employee',[('id','=',ValGeneralData[i]['empleado_id'][0]),])

            ValGeneralData[i]['empleado'] = ValGeneralData[i]['empleado_id'][1]
            ValGeneralData[i]['ciclo_limpio'] = ValGeneralData[i]['configfecha_id'][1]
            employeehr= self.instanciar_objetos('hr.employee',[('id','=',ValGeneralData[i]['empleado_id'][0]),])
            if employeehr.active is True:
                ValGeneralData[i]['activo'] = 'SI'
                ValGeneralData[i]['departamento'] = employeehr.department_id.name
            else:
                ValGeneralData[i]['activo'] = 'NO'
                ValGeneralData[i]['departamento'] = employeehr.department_id.name


            if ValGeneralData[i]['state'] == 'inicio':
                ValGeneralData[i]['estatus'] = 'Inicio'
            elif ValGeneralData[i]['state'] == 'recibida':
                ValGeneralData[i]['estatus'] = 'Recibida por Analista'
            elif ValGeneralData[i]['state'] == 'revisada':
                ValGeneralData[i]['estatus'] = 'Revisada por Analista'
            elif ValGeneralData[i]['state'] == 'enviada':
                ValGeneralData[i]['estatus'] = 'Enviada a RRHH'
            else:
                ValGeneralData[i]['estatus'] = 'Evaluacion Validada'



            i += 1

        return http.request.render('evaluaciones.historial_empleado',
            {'datos':ValGeneralData
            })

    @http.route(['/historial/detalle/<int:id>'], type='http', auth="user", website=True)
    def historial_detalles(self,id):
        registry = http.request.registry
        cr=http.request.cr
        uid=http.request.uid
        context = http.request.context
        id_empleado = id

        evaluaciones_obj = registry.get("fci_gh.eva_empleados")
        evaluaciones_line_obj = registry.get("fci_gh.eva_empleados.line")

        evaluacion_id=evaluaciones_obj.search(cr,uid,[('id','=',id)],context=context)

        #quitar luego
        evaluacion_line_id=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id','=',id)],context=context)
        ValGeneralData_line = evaluaciones_line_obj.browse(cr,uid,evaluacion_line_id,context)
        ######

        ValGeneralData = evaluaciones_obj.browse(cr,uid,evaluacion_id,context)

        #######################################ODI################################################

        odi_line_id=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id','=',id)],context=context)
        odi_Data = evaluaciones_line_obj.read(cr,uid,odi_line_id,[
                                                'actividades',
                                                'peso',
                                                'subtotal',
                                                'rangocualitativo_id'
                                                ],context=context)
        maximo = len(odi_Data)
        i = 0
        while i < maximo:
            if ValGeneralData.empleado_id.cargo_id.cargo == 'ANALISTA':
                odi_Data[i]['rango'] = odi_Data[i]['rangocualitativo_id'][1].split('/')[1]
            else:
                odi_Data[i]['rango'] = odi_Data[i]['rangocualitativo_id'][1].split('/')[0]

            i += 1

        #######################################Competencia################################################
        competencias_line_id1=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id1','=',id)],context=context)
        competencias_Data = evaluaciones_line_obj.read(cr,uid,competencias_line_id1,[
                                                'competencia_id',
                                                'peso',
                                                'subtotal',
                                                'rangocualitativo_id'
                                                ],context=context)



        maximo = len(competencias_Data)
        i = 0
        while i < maximo:
            competencias_Data[i]['competencia'] = competencias_Data[i]['competencia_id'][1]
            if ValGeneralData.empleado_id.cargo_id.cargo == 'ANALISTA':
                competencias_Data[i]['rango'] = competencias_Data[i]['rangocualitativo_id'][1].split('/')[1]
            else:
                competencias_Data[i]['rango'] = competencias_Data[i]['rangocualitativo_id'][1].split('/')[0]

            i += 1
        evaluaciones_data = evaluaciones_obj.read(cr,uid,evaluacion_id,[
                                                        'empleado_id',
                                                        ],context=context)
        #######################################Competencia Tecnica################################################
        competencias_line_id2=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id2','=',id)],context=context)
        tecnica_Data = evaluaciones_line_obj.read(cr,uid,competencias_line_id2,[
                                                'competencia_id',
                                                'peso',
                                                'subtotal',
                                                'rangocualitativo_id',
                                                ],context=context)





        maximo = len(tecnica_Data)
        i = 0
        while i < maximo:
            tecnica_Data[i]['competencia'] = tecnica_Data[i]['competencia_id'][1]
            if ValGeneralData.empleado_id.cargo_id.cargo == 'ANALISTA':
                tecnica_Data[i]['rango'] = tecnica_Data[i]['rangocualitativo_id'][1].split('/')[0]
            else:
                tecnica_Data[i]['rango'] = tecnica_Data[i]['rangocualitativo_id'][1].split('/')[1]



            i += 1



        evaluaciones_data = evaluaciones_obj.read(cr,uid,evaluacion_id,[
                                                        'empleado_id',
                                                        ],context=context)


#######################Departamento#######################################################################
        empleado_obj = registry.get("hr.employee")
        empleado_id = empleado_obj.search(cr,uid,[('id','=',evaluaciones_data[0]['empleado_id'][0])],context=context)
        empleado_data = empleado_obj.browse(cr,uid,empleado_id,context)
        department = empleado_obj.read(cr,uid,empleado_id,['department_id',],context=context)
        dept = department[0]['department_id'][0]
        empleado_obj2 = registry.get("hr.department")
        empleado_id2 = empleado_obj2.search(cr,uid,[('id','=',dept)],context=context)
        empleado_data2 = empleado_obj2.browse(cr,uid,empleado_id2,context)
########################Evaluador#################################################

        #evaluador = empleado_obj.read(cr,uid,empleado_id,['parent_id',],context=context)
        #evaluador_obj = evaluador[0]['parent_id'][1]

########################Director#######################################################

        #director = empleado_obj.read(cr,uid,empleado_id,['coach_id',],context=context)
        #director_obj = director[0]['coach_id'][1]
########################TipoCargo#######################################################
        cargo = empleado_obj.read(cr,uid,empleado_id,['cargo_id',],context=context)

        if cargo[0]['cargo_id']:
            cargo_obj = cargo[0]['cargo_id'][0]

            tipo_cargo_obj = registry.get("fci_gh.con_cargo")
            tipo_cargo_id = tipo_cargo_obj.search(cr,uid,[('id','=',cargo_obj)],context=context)
            cargo_data = tipo_cargo_obj.browse(cr,uid,tipo_cargo_id,context)

            cargo_def = cargo_data[0]['cargo']
        else:
            cargo_def = 'Analista'


########################CicloEvaluacion#######################################################



        ciclo_data = evaluaciones_obj.read(cr,uid,evaluacion_id,['configfecha_id',],context=context)


        ciclo =  ciclo_data[0]['configfecha_id'][1]
########################Criterios#########################################################
        criterio_line_id=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id3','=',id)],context=context)
        criterio_Data = evaluaciones_line_obj.read(cr,uid,criterio_line_id,[
                                                'actividades1',
                                                ],context=context)


########################Logros#########################################################
        logros_line_id=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id4','=',id)],context=context)
        logros_Data = evaluaciones_line_obj.read(cr,uid,logros_line_id,[
                                                'actividades2',
                                                ],context=context)

########################Impacto#########################################################
        impacto_line_id=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id5','=',id)],context=context)
        impacto_Data = evaluaciones_line_obj.read(cr,uid,impacto_line_id,[
                                                'actividades3',
                                                ],context=context)
########################Institucional#########################################################
        institucional_line_id=evaluaciones_line_obj.search(cr,uid,[('eva_empleados_line_id6','=',id)],context=context)
        institucional_Data = evaluaciones_line_obj.read(cr,uid,institucional_line_id,[
                                                'actividades4',
                                                ],context=context)
        valor = ValGeneralData.actuacion
        eva_data=self.instanciar_objetos('fci_gh.eva_empleados',[('id','=',id)])


        tipo_evaluacion = eva_data.empleado_id.cargo_id.tipoevaluacion_id.nombre_evaluacion
        if tipo_evaluacion is False:
            tipo_evaluacion = ValGeneralData.tipo_evaluacion

        if ValGeneralData.empleado_id.cargo_id.cargo == 'ANALISTA':
            if ValGeneralData['actuacion'] == 'Desempeño Excepcional':
                actuacion = ValGeneralData['actuacion']
            else:
                actuacion = ValGeneralData['actuacion'].split("/")[0]
        else:
            if ValGeneralData['actuacion'] == 'Desempeño Excelente':
                actuacion = ValGeneralData['actuacion']
            else:
                actuacion = ValGeneralData['actuacion'].split("/")[1]

        return http.request.render('evaluaciones.historial_detalle',
                    {   'ValGeneralData':ValGeneralData,
                        'actuacion':actuacion,
                        'ValGeneralData_line':ValGeneralData_line,
                        'odi_Data':odi_Data,
                        'competencias_Data':competencias_Data,
                        'tecnica_Data':tecnica_Data,
                        'tipo_evaluacion':tipo_evaluacion,
                        'criterio_Data':criterio_Data,
                        'logros_Data':logros_Data,
                        'impacto_Data':impacto_Data,
                        'institucional_Data':institucional_Data,
                        'empleado_data':empleado_data, #PASO ESTE PARAMETRO A LA VISTA
                        'empleado_data2':empleado_data2,
                        #'empleado_data3':evaluador_obj,
                        #'empleado_data4':director_obj,
                        'cargo_data':cargo_def,
                        'ciclo':ciclo,
                        'valor':valor
                        })
