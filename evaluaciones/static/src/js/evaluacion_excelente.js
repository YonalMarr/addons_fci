function add_excep(tipo){
    ciclo = $("input:hidden[name=ciclo]").val();
    empleado = $("input:hidden[name=empleado]").val();
    tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();
    $.post( "/evaluaciones/contar_exc",
    {ciclo: ciclo,
    empleado: empleado,
    tipoevaluacion:tipoevaluacion,
    tipo:tipo,
    },
    function(data){
        x = parseInt(data['numero']) + 1;
        var input ='<tr id="'+tipo+'_tr'+x+'">'
        +'<input type="hidden" id="'+tipo+'_id_'+x+'" name="'+tipo+'_id_'+x+'" />'
        +'<td>'

      +'<div id="'+tipo+'-span_'+x+'" name="'+tipo+'-span_'+x+'" style="display:none;">'
      +'<span name="_'+tipo+'_'+x+'"/>'
      +'</div>'
      +'<div id="'+tipo+'-input_'+x+'" name="'+tipo+'-input_'+x+'">'
        +'<div class="mui-textfield"><input type="text" id="'+tipo+'_'+x+'" name="'+tipo+'_'+x+'" style="width: 100%;" class="form-control"/>'
        +'</div></div></td>'
        +'<td>'
            +'<button class="btn teal" name="edit_'+tipo+'_'+x+'" id="edit_'+tipo+'_'+x+'" onclick="edit_excep(this.id)" style="display: none;">'
              +'<i class="material-icons">mode_edit</i>'
            +'</button>'
              +'<button class="btn teal" name="guardar_'+tipo+'_'+x+'" id="guardar_'+tipo+'_'+x+'"  onclick="guardar_excep(this.id)">'
                +'<i class="material-icons">save</i>'
              +'</button>'
            +'</td>'
            +'<td style="border:0px solid #fff;">'
              +'<button class="btn teal" name="erase_'+tipo+'_'+x+'" id="erase_'+tipo+'_'+x+'" onclick="delete_excep(this.id)" style="display: none;">'
                +'<i class="material-icons">delete</i>'
              +'</button>'

              +'<button class="btn teal" name="cancelar_'+tipo+'_'+x+'" id="cancelar_'+tipo+'_'+x+'"  onclick="cancelar_excep()">'
                +'<i class="material-icons">not_interested</i>'
              +'</button>'
        +'</td>'
        +'</tr>';
        $("#"+tipo+"s").append(input);
    },"json");
}

function edit_excep(id){
        var edit = id;
        edit = edit.split("_")
        var z = edit[2];
        var tipo = edit[1];
        $("input:text[name="+tipo+"_"+z+"]").removeAttr("readonly");

        $("#"+tipo+"-span_"+z).hide();
        $("#"+tipo+"-input_"+z).show();

        $("#"+id).hide();
        $("#erase_"+tipo+"_"+z).hide();
        $("#guardar_"+tipo+"_"+z).show();
        $("#cancelar_"+tipo+"_"+z).show();
}



function delete_excep(id){
    var edit = id;
    edit = edit.split("_")
    var z = edit[2];
    var tipo = edit[1];
    id_line = $("input:hidden[name="+tipo+"_id_"+z+"]").val();
    empleado = $("input:hidden[name=empleado]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    $.post( "/evaluaciones/crear/exec/eliminar",
        {id_line: id_line,
        empleado:empleado,
        ciclo:ciclo,
        },
        function(data){
            if(data['valor'] == true){
                //window.location.href = "/evaluaciones/"+empleado;
                $('#'+tipo+"_tr"+z).remove();

            }
        },"json"
    );

}

function cancelar_excep(){
    empleado = $("input:hidden[name=empleado]").val();
    window.location.href = "/evaluaciones/"+empleado;
}

function guardar_excep(id){
    var edit = id;
    edit = edit.split("_")
    var z = edit[2];
    var tipo = edit[1];
    act = $("input:text[name="+tipo+"_"+z+"]").val();
    id_line = $("input:hidden[name="+tipo+"_id_"+z+"]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    empleado = $("input:hidden[name=empleado]").val();

    if(id_line == null || id_line == ''){id_line = 0}


    $.post( "/evaluaciones/crear/excelente/guardar",
        {act: act,
        id_line: id_line,
        ciclo: ciclo,
        empleado: empleado,
        tipo:tipo,
        },
        function(data){
            if(data['mensaje'] == true){
                return false;
            }else{
                $("input:text[name="+tipo+"_"+z+"]").attr("readonly","true");

                $("#"+tipo+"-input_"+z).hide();
                $('#'+tipo+'-span_'+z+' span').text(act);
                $("#"+tipo+"-span_"+z).show();

                $("#guardar_"+tipo+"_"+z).hide();
                $("#edit_"+tipo+"_"+z).show();
                $("#erase_"+tipo+"_"+z).show();
                $("#cancelar_"+tipo+"_"+z).hide();
                $("input:hidden[name="+tipo+"_id_"+z+"]").val(data['id_line'])
                //$("#"+tipo+"s").load();

                }
                return false;
            },"json");
}





















function enviar_ure(){
  empleado = $("input:hidden[name=empleado]").val();
  ciclo = $("input:hidden[name=ciclo]").val();
  actuacion = $("input:text[name=actuacion]").val();
  tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();

          $.post( "/evaluaciones/ure/",{
              empleado: empleado,
              ciclo:ciclo,
              actuacion:actuacion,
              tipoevaluacion:tipoevaluacion
            //  recomendacion:recomendacion
          },
              function(datas){
                  if(datas['mensaje'] == 'NO'){
                        window.location.href = "/evaluaciones";
                    }else{
                      swal({
                      title: '¡Error!',
                      text: 'Faltan Campos por Completar o Por Evaluar',
                      type: 'error',
                      confirmButtonText: 'OK'});
                  }
              },"json");
  }




function enviar_analista(){
    empleado = $("input:hidden[name=empleado]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    actuacion = $("input:text[name=actuacion]").val();
    tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();

        $.post( "/evaluaciones/enviar/",{
            empleado: empleado,
            ciclo:ciclo,
            actuacion:actuacion,
            tipoevaluacion:tipoevaluacion
          //  recomendacion:recomendacion
        },
            function(datas){
                if(datas['mensaje'] == 'NO'){
                    swal({
                        title: "Recomendaciones y/o Capacitaciones",
                        text: "Sugeridas por el Evaluador",
                        type: "input",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        animation: "slide-from-top",
                        inputPlaceholder: "" },
                        function(inputValue){
                            if (inputValue === false)
                                return false;
                            if (inputValue === "") {
                                swal.showInputError("Es necesario escribir");
                                return false   }
                            $.post("/evaluaciones/recomendacion/",{
                                empleado: empleado,
                                ciclo:ciclo,
                                actuacion:actuacion,
                                tipoevaluacion:tipoevaluacion,
                                recomendacion:inputValue
                            },function(data){
                                if(data['mensaje'] == 'SI'){
                                   window.location.href = "/evaluaciones";
                                }

                            });
                            window.location.href = "/evaluaciones";

                            //swal("Nice!", "You wrote: " + inputValue, "success");
                        });

                }else{
                    swal({
                    title: '¡Error!',
                    text: 'Faltan Campos por Completar o Por Evaluar',
                    type: 'error',
                    confirmButtonText: 'OK'});
                }
            },"json");
}

function enviar_evaluador(){
    empleado = $("input:hidden[name=empleado]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    actuacion = $("input:text[name=actuacion]").val();
    tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();
    observacion =  $("input:text[name=observaciones]").val();


        $.post( "/evaluaciones/revisar/",{
            empleado: empleado,
            ciclo:ciclo,
            actuacion:actuacion,
            tipoevaluacion:tipoevaluacion,
            observacion:observacion
        },
            function(data){
                if(data['valor'] == true){

                    window.location.href = "http://gestion.cfg.gob.ve";
                    /*CREAR PAGINA CON IMAGEN DE ENVIADA*/
                }else{
                    swal({
                    title: '¡Error!',
                    text: 'Debe escribir alguna observación',
                    type: 'error',
                    confirmButtonText: 'OK'});
                }
            },"json");

}

function enviar_rrhh(){
    empleado = $("input:hidden[name=empleado]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    actuacion = $("input:text[name=actuacion]").val();
    tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();
    observacion =  $("input:text[name=observaciones]").val();


        $.post( "/evaluaciones/rrhh/",{
            empleado: empleado,
            ciclo:ciclo,
            actuacion:actuacion,
            tipoevaluacion:tipoevaluacion,
            observacion:observacion
        },
            function(data){
                if(data['valor'] == true){

                    window.location.href = "/evaluaciones";
                }else{
                    swal({
                    title: '¡Error!',
                    text: 'Debe escribir alguna observación',
                    type: 'error',
                    confirmButtonText: 'OK'});
                }
            },"json");

}

function regresar_eva(){
    empleado = $("input:hidden[name=empleado]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    actuacion = $("input:text[name=actuacion]").val();

        $.post( "/evaluaciones/regresar/",{
            empleado: empleado,
            ciclo:ciclo,
            actuacion:actuacion
        },
            function(data){
                if(data['mensaje'] == 'NO'){
                    window.location.href = "/revision";
                }else{
                    swal({
                    title: '¡Error!',
                    text: 'Error',
                    type: 'error',
                    confirmButtonText: 'OK'});
                }
            },"json");
}

function validar_eva(){
    empleado = $("input:hidden[name=empleado]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    actuacion = $("input:text[name=actuacion]").val();

        $.post( "/evaluaciones/validar/",{
            empleado: empleado,
            ciclo:ciclo,
            actuacion:actuacion
        },
            function(data){
                if(data['mensaje'] == 'NO'){
                    window.location.href = "/revision";
                }else{
                    swal({
                    title: '¡Error!',
                    text: 'Error',
                    type: 'error',
                    confirmButtonText: 'OK'});                }
            },"json");

}
