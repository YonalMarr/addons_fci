$(document).ready(function(){
  $('.striped').DataTable();
  $('select').material_select();

  $('.button-collapse').sideNav({
      menuWidth: 260, // Default is 240
      edge: 'left' // Choose the horizontal origin
      //closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      //draggable: false // Choose whether you can drag to open on touch screens
  });

  $('.modal').modal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
    });
});
