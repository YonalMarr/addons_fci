function delete_odi(id){
var edit = id; 
        edit = edit.split("_")
        var z = edit[1];
        id_line = $("input:hidden[name=id_"+z+"]").val();
        empleado = $("input:hidden[name=empleado]").val();
        ciclo = $("input:hidden[name=ciclo]").val();
        $.post( "/evaluaciones/crear/odi/eliminar",
            {id_line: id_line,
            empleado:empleado,
            ciclo:ciclo,
            },
            function(data){
                if(data['valor'] == true){
                    //window.location.href = "/evaluaciones/"+empleado;

                $("input:text[name=actuacion]").val(data['actuacion']);
                $("input:text[name=total_comp]").val(data['total_comp']);
                $("input:text[name=total_tec]").val(data['total_tec']);
                $("input:text[name=total_eva]").val(data['total_eva']);
                $("input:text[name=total_odi]").val(data['total_odi']);
                    
                    $('#'+z).remove();

                }
            },"json"
        );

}

function edit(id){
        var edit = id; 
        edit = edit.split("_")
        var z = edit[1];
        tipoEmpleado = $("input:hidden[name=tipoevaluacion]").val();


        $("input:text[name=actividad_"+z+"]").removeAttr("readonly");
        $("input:text[name=peso_"+z+"]").removeAttr("readonly");
        $("select[name=rango_"+z+"]").removeAttr("disabled");

        $("#actividad-span_"+z).hide();
        $("#actividad-input_"+z).show();

        $("#select-span_"+z).hide();
        $("#select-input_"+z).show();

        $("#peso-span_"+z).hide();
        $("#peso-input_"+z).show();

        $("#"+id).hide();
        $("#erase_"+z).hide();
        $("#guardar_"+z).show();
        $("#cancelar_"+z).show();

}
function guardar(id){
        var edit = id;
        edit = edit.split("_")
        var z = edit[1];

        act = $("input:text[name=actividad_"+z+"]").val();
        peso = $("input:text[name=peso_"+z+"]").val();
        rango = $("select[name=rango_"+z+"]").val();
        subtotal = $("input:text[name=subtotal_"+z+"]").val();

        id_line = $("input:hidden[name=id_"+z+"]").val();
        ciclo = $("input:hidden[name=ciclo]").val();
        empleado = $("input:hidden[name=empleado]").val();

        if(peso == ''){peso = 0;}
        if(subtotal == ''){subtotal = 0;}
        
        if(id_line == ''){id_line = 0}

        $.post( "/evaluaciones/crear/odi/guardar",
        {act: act, 
        peso: peso,
        rango: rango,
        subtotal: subtotal,
        id_line: id_line,
        ciclo: ciclo,
        empleado: empleado,
        },

        function(data){
            if(data['mensaje'] == true){
                swal({
                  title: '¡Error!',
                  text: 'La Suma de Los Pesos es Mayor a 50!',
                  type: 'error',
                  confirmButtonText: 'OK'
                }); 
                return false; 
            }else{
                $("input:text[name=actividad_"+z+"]").attr("readonly","true");
                $("input:text[name=peso_"+z+"]").attr("readonly","true");
                $("select[name=rango_"+z+"]").attr("disabled","true");

                $("#guardar_"+z).hide();
                $("#edit_"+z).show();
                $("#erase_"+z).show();
                $("#cancelar_"+z).hide();
                //$("#odis").load();
                
                $("#actividad-input_"+z).hide();
                $('#actividad-span_'+z+' span').text(act);
                $("#actividad-span_"+z).show();

                $("#select-input_"+z).hide();
                $('#select-span_'+z+' span').text(data['rango']);
                $("#select-span_"+z).show();
                
                $("#peso-input_"+z).hide();
                $('#peso-span_'+z+' span').text(peso);
                $("#peso-span_"+z).show();
                
                $("input:text[name=actuacion]").val(data['actuacion']);
                $("input:text[name=total_eva]").val(data['total_eva']);
                $("input:text[name=total_odi]").val(data['total_odi']);
                $("input:hidden[name=id_"+z+"]").val(data['id_line']);
                if(data['actuacion'] == 'Desempeño Excepcional' || data['actuacion'] == 'Desempeño Excelente'){
                swal({
                  title: data['actuacion'],
                  text: "Proceder a completar la información",
                  type: 'info',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'OK'
                }, function () {
                    window.location.href = "/evaluaciones/"+empleado;
                });
                }else{
                    $( "div" ).remove( ".excepcional" );


                }
                return false;  
            }
           
        },"json");

       

}

function add_record(){
	
    ciclo = $("input:hidden[name=ciclo]").val();
    empleado = $("input:hidden[name=empleado]").val();

    $.post( "/evaluaciones/contar_odi",
    {ciclo: ciclo,
    empleado: empleado,
    },
    function(data){
        x = parseInt(data['numero']) + 1;
        rango = 0;
        var rangos = "";
        while (rango < parseInt(data['max_rango'])) {
               rangoStr = data['rango'][rango].split("/")
               if(data['cargo'] == 'ANALISTA'){rangoCut = rangoStr[1]}else{rangoCut = rangoStr[0]}
               rangos = rangos.concat('<option value="'+data['valor'][rango]+'">'+rangoCut+'</option>');
               rango = rango + 1;
            } 

        var input ='<tr id="'+x+'"><input type="hidden" name="id_'+x+'"  value="" id="id_'+x+'" class="validate" style="width: 100%;"/><td>'
	    +'<div id="actividad-span_'+x+'" name="actividad-span_'+x+'" style="display:none;">'
        +'<span name="actividad_s_'+x+'"/></div>'
        +'<div id="actividad-input_'+x+'" name="actividad-input_'+x+'" >'
        +'<div class="input-field">'
        +'<input type="text" id="actividad_'+x+'" name="actividad_'+x+'"  style="width: 100%;" class="validate" />'
        +'</div></div></td>'

        +'<td>'
        +'<div id="peso-span_'+x+'" name="peso-span_'+x+'" style="display:none;">'
        +'<span name="peso_s_'+x+'"/></div>'
        +'<div id="peso-input_'+x+'" name="peso-input_'+x+'" >'
        +'<div class="input-field"><input type="text" id="peso_'+x+'" name="peso_'+x+'" style="width: 100%;" class="validate"  onchange="myFunction(this.id)"/></div></td>'
        +'</div>'
    
        +'<td>'
        +'<div id="select-span_'+x+'" name="select-span_'+x+'" style="display:none;">'
        +'<span name="select_s_'+x+'"/></div>'
        +'<div id="select-input_'+x+'" name="select-input_'+x+'" >'
        +'<div class="input-field"><select class="browser-default" name="rango_'+x+'" id="rango_'+x+'" onchange="myFunction(this.id)">'
             +rangos
        +'</select></div>'
        +'</div>'
        +'</td>'

        +'<td><div class="input-field"><input type="text" id="subtotal_'+x+'" name="subtotal_'+x+'" style="width: 100%;" class="validate" readonly/>'
        +'</div></td>'
        +'<td><table>'
            +'<tr style="border:0px solid #fff;">'
            +'<td  style="border:0px solid #fff;">'
              +'<button class="btn teal" name="edit_'+x+'" id="edit_'+x+'" style="display: none;" onclick="edit(this.id)">'
                +'<i class="material-icons">mode_edit</i>'
              +'</button>'

              +'<button class="btn teal" name="guardar_'+x+'" id="guardar_'+x+'" onclick="guardar(this.id)">'
                +'<i class="material-icons">save</i>'
              +'</button>'
            +'</td>'
            +'<td  style="border:0px solid #fff;">'
              +'<button class="btn teal" name="erase_'+x+'" id="erase_'+x+'" style="display: none;" onclick="delete_odi(this.id)">'
                +'<i class="material-icons">delete</i>'
              +'</button>'

              +'<button class="btn teal" name="cancelar_'+x+'" id="cancelar_'+x+'" onclick="cancelar()">'
                +'<i class="material-icons">not_interested</i>'
              +'</button>'
            +'</td>'
            +'</tr>'
            +'</table></td>'
        +'</tr>';
        $("#odis").append(input); 
        //$("#odis").load();
        },"json");

}

function cancelar(){
empleado = $("input:hidden[name=empleado]").val();
                window.location.href = "/evaluaciones/"+empleado;

}
