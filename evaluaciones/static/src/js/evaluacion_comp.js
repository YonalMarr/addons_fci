

function edit_c(id){

        var edit = id;
        edit = edit.split("_")
        var z = edit[1];


        $("#peso-select_"+z).show();
        $("#peso-span_"+z).hide();

        $("#rango-select_"+z).show();
        $("#rango-span_"+z).hide();

        $("select[name=competencia_"+z+"]").removeAttr("disabled");
        $("input:text[name=peso_"+z+"]").removeAttr("readonly");
        $("select[name=rango_"+z+"]").removeAttr("disabled");

        $("#"+id).hide();
        $("#erase_"+z).hide();
        $("#guardar_"+z).show();
        $("#cancelar_"+z).show();

}

function delete_comp(id){
    var edit = id;
    edit = edit.split("_")
    var z = edit[1];
    id_line = $("input:hidden[name=id_"+z+"]").val();
    empleado = $("input:hidden[name=empleado]").val();
    ciclo = $("input:hidden[name=ciclo]").val();
    $.post( "/evaluaciones/crear/odi/eliminar",
        {id_line: id_line,
        empleado:empleado,
        ciclo:ciclo,
        },
    function(data){
    if(data['valor'] == true){

                $("input:text[name=actuacion]").val(data['actuacion']);
                $("input:text[name=total_comp]").val(data['total_comp']);
                $("input:text[name=total_tec]").val(data['total_tec']);
                $("input:text[name=total_odi]").val(data['total_odi']);
                $("input:text[name=total_eva]").val(data['total_eva']);
        //window.location.href = "/evaluaciones/"+empleado
        $('#'+z).remove();

    }
    },"json"
    );
}

function guardar_c(id){
        var edit = id;
        edit = edit.split("_")
        var z = edit[1];


        act = $("select[name=competencia_"+z+"]").val();
        rango = $("select[name=rango_"+z+"]").val();
        subtotal = $("input:text[name=subtotal_"+z+"]").val();
        id_line = $("input:hidden[name=id_"+z+"]").val();
        ciclo = $("input:hidden[name=ciclo]").val();
        empleado = $("input:hidden[name=empleado]").val();
        peso = $("input:text[name=peso_"+z+"]").val();
        tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();


        if(peso == ''){peso = 0;}
        if(subtotal == ''){subtotal = 0;}

        if(id_line == null){id_line = 0}

        $.post( "/evaluaciones/crear/comp/guardar",
        {act: act,
        rango: rango,
        subtotal: subtotal,
        id_line: id_line,
        ciclo: ciclo,
        empleado: empleado,
        peso:peso,
        tipoevaluacion:tipoevaluacion,
        },

        function(data){
            if(data['mensaje'] == true){

                swal({
                  title: '¡Error!',
                  text: 'La Suma de Los Pesos es Mayor a 50!',
                  type: 'error',
                  confirmButtonText: 'OK'
                });
                return false;
            }else{
                $("select[name=competencia_"+z+"]").attr("disabled","true");
                $("select[name=rango_"+z+"]").attr("disabled","true");
                $("input:text[name=peso_"+z+"]").attr("readonly","true");

                $("#guardar_"+z).hide();
                $("#edit_"+z).show();
                $("#erase_"+z).show();
                $("#cancelar_"+z).hide();

                //$("#competencias").load();

                $("#competencia-select_"+z).hide();
                $('#competencia-span_'+z+' span').text(data['selc']);
                $("#competencia-span_"+z).show();

                $("#peso-select_"+z).hide();
                $('#peso-span_'+z+' span').text(peso);
                $("#peso-span_"+z).show();

                $("#rango-select_"+z).hide();
                $('#rango-span_'+z+' span').text(data['rango']);
                $("#rango-span_"+z).show();

                $("input:text[name=actuacion]").val(data['actuacion']);
                $("input:text[name=total_comp]").val(data['total_comp']);
                $("input:text[name=total_tec]").val(data['total_tec']);
                $("input:text[name=total_eva]").val(data['total_eva']);
                $("input:hidden[name=id_"+z+"]").val(data['id_line']);
                if(data['actuacion'] == 'Desempeño Excepcional' || data['actuacion'] == 'Desempeño Excelente'){
                swal({
                  title: data['actuacion'],
                  text: "Proceder a completar la información",
                  type: 'info',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'OK'
                },function () {
                    window.location.href = "/evaluaciones/"+empleado;

                });


                }else{
                    $( "div" ).remove( ".excepcional" );

                }

                return false;
            }

        },"json");



}

function add_record_c(){
    ciclo = $("input:hidden[name=ciclo]").val();
    empleado = $("input:hidden[name=empleado]").val();
    tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();
    $.post( "/evaluaciones/contar_comp",
    {ciclo: ciclo,
    empleado: empleado,
    tipoevaluacion:tipoevaluacion
    },
    function(data){
        x = parseInt(data['numero']) + 1;
        rango = 0;
        var rangos = "";

        while (rango < parseInt(data['max_rango'])) {
               rangos = rangos.concat('<option value="'+data['valor'][rango]+'">'+data['rango'][rango].split("/")[1]+'</option>');
               rango = rango + 1;
            }
        competencia = 0;
        var competencias = "";
        while(competencia < parseInt(data['max_competencia'])){
            competencias = competencias.concat('<option value="'+data['id_comp'][competencia]+'">'+data['competencia'][competencia]+'</option>');
            competencia = competencia + 1;
        }
        var input ='<tr id="c'+x+'"><input type="hidden" name="id_c'+x+'" value="0" id="id_c" class="form-control" style="width: 100%;"/><td style="width:65%;">'
        +'<div id="competencia-span_c'+x+'" name="competencia-span_c'+x+'" style="display: none;">'
        +'<span name="comp_act_c'+x+'"/></div>'
        +'<div id="competencia-select_c'+x+'" name="competencia-select_c'+x+'">'

        +'<select class="browser-default" name="competencia_c'+x+'" id="competencia_c'+x+'">'
             +competencias
        +'</select>'
        +'</div>'
        +'</td>'

        +'<td style="width:5%;"><div class="mui-textfield">'
        +'<input type="text" name="peso_c'+x+'" id="peso_c'+x+'" class="form-control" style="width: 100%;"  onchange="myFunction(this.id)"/>'
        +'</div></td>'

        +'<td style="width:15%;">'
        +'<div id="rango-span_c'+x+'" name="rango-span_c'+x+'" style="display: none;">'
        +'<span name="comp_rango_c'+x+'"/></div>'
        +'<div id="rango-select_c'+x+'" name="rango-select_c'+x+'">'

        +'<select class="browser-default" name="rango_c'+x+'" id="rango_c'+x+'" onchange="myFunction(this.id)">'
             +rangos
        +'</select>'
        +'</div>'

        +'</td>'

        +'<td style="width:5%;"><div class="mui-textfield">'
        +'<input type="text" id="subtotal_c'+x+'" name="subtotal_c'+x+'" style="width: 100%;" class="form-control" readonly/>'
        +'</div></td>'

        +'<td style="width:10%;"><table>'
            +'<tr style="border:0px solid #fff;">'
            +'<td style="border:0px solid #fff;">'
                +'<button class="btn teal" name="edit_c'+x+'" id="edit_c'+x+'" style="display: none;" onclick="edit_c(this.id)">'
                +'<i class="material-icons">mode_edit</i>'
                +'</button>'
                +'<button class="btn teal" name="guardar_c'+x+'" id="guardar_c'+x+'" onclick="guardar_c(this.id)">'
                    +'<i class="material-icons">save</i>'
                +'</button>'
            +'</td>'
            +'<td  style="border:0px solid #fff;">'
              +'<button class="btn teal" name="erase_c'+x+'" id="erase_c'+x+'" style="display: none;" onclick="delete_comp(this.id)">'
                +'<i class="material-icons">delete</i>'
              +'</button>'

              +'<button class="btn teal" name="cancelar_c'+x+'" id="cancelar_c'+x+'" onclick="cancelar_c()">'
                +'<i class="material-icons">not_interested</i>'
              +'</button>'
            +'</td>'
            +'</tr>'
            +'</table></td>'
        +'</tr>';
        $("#competencias").append(input);
        //$("#competencias").load();

        },"json");

}


function add_record_ct(){

    ciclo = $("input:hidden[name=ciclo]").val();
    empleado = $("input:hidden[name=empleado]").val();
    tipoevaluacion =  $("input:hidden[name=tipoevaluacion]").val();
    $.post( "/evaluaciones/contar_comp",
    {ciclo: ciclo,
    empleado: empleado,
    tipoevaluacion:tipoevaluacion
    },
    function(data){
        x = parseInt(data['numero']) + 1;
        rango = 0;
        var rangos = '<option value="">Seleccione...</option>';

        while (rango < parseInt(data['max_rango'])) {
               rangos = rangos.concat('<option value="'+data['valor'][rango]+'">'+data['rango'][rango].split("/")[0]+'</option>');
               rango = rango + 1;
            }

        competencia = 0;
        var competencias = "";
        while(competencia < parseInt(data['max_competencia'])){
            competencias = competencias.concat('<option value="'+data['id_comp'][competencia]+'">'+data['competencia'][competencia]+'</option>');
            competencia = competencia + 1;
        }
        var input ='<tr id="c'+x+'"><input type="hidden" name="id_c'+x+'" value="0" id="id_c" class="form-control" style="width: 100%;"/><td style="width:65%;">'

        +'<div id="competencia-span_c'+x+'" name="competencia-span_c'+x+'" style="display: none;">'
        +'<span name="comp_act_c'+x+'"/></div>'
        +'<div id="competencia-select_c'+x+'" name="competencia-select_c'+x+'">'
        +'<select class="browser-default" name="competencia_c'+x+'" id="competencia_c'+x+'">'
             +competencias
        +'</select>'
        +'</div>'
        +'</td>'
        +'<td style="width:15%;">'
        +'<div id="rango-span_c'+x+'" name="rango-span_c'+x+'" style="display: none;">'
        +'<span name="comp_rango_c'+x+'"/></div>'
        +'<div id="rango-select_c'+x+'" name="rango-select_c'+x+'">'

        +'<select class="browser-default" name="rango_c'+x+'" id="rango_c'+x+'" onchange="myFunction2(this.id)">'
             +rangos
        +'</select>'
        +'</div></td>'
        +'<td style="width:10%;"><div class="mui-textfield">'
        +'<input type="text" id="subtotal_c'+x+'" name="subtotal_c'+x+'"  class="form-control" readonly/>'
        +'</div></td>'
        +'<td style="width:10%;"><table>'
            +'<tr style="border:0px solid #fff;">'
            +'<td style="border:0px solid #fff;">'
                +'<button class="btn teal" name="edit_c'+x+'" id="edit_c'+x+'" style="display: none;" onclick="edit_c(this.id)">'
                    +'              <i class="material-icons">mode_edit</i>'
                +'</button>'
                +'<button class="btn teal" name="guardar_c'+x+'" id="guardar_c'+x+'" onclick="guardar_c(this.id)">'
                    +'<i class="material-icons">save</i>'
                +'</button>'
            +'</td>'
            +'<td  style="border:0px solid #fff;">'
              +'<button class="btn teal" name="erase_c'+x+'" id="erase_c'+x+'" style="display: none;" onclick="delete_comp(this.id)">'
                +'<i class="material-icons">delete</i>'
              +'</button>'

              +'<button class="btn teal" name="cancelar_c'+x+'" id="cancelar_c'+x+'" onclick="cancelar_c()">'
                +'<i class="material-icons">not_interested</i>'
              +'</button>'
            +'</td>'
            +'</tr>'
            +'</table></td>'
        +'</tr>';
        $("#competencias_tec").append(input);
       // $("#competencias").load();

        },"json");

}

function cancelar_c(){

empleado = $("input:hidden[name=empleado]").val();
window.location.href = "/evaluaciones/"+empleado;

}
