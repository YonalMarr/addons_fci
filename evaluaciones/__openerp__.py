#-*-coding: utf-8-*-
{
    'name':'fci_evaluaciones_website',
    'version':'1.0',
    'depends':['base','base_setup','website', 'website_apiform'],
    'author':'Coordinación de Sistemas - CFG/FCI',
    'category':'',
    'description':"""Sistema web de evaluaciones""",
    'update_xml':[],
    'data':[
      'vista/template_header.xml',
      'vista/template_header_evaluaciones.xml',
      'vista/template_principal.xml',
      'vista/template_footer.xml',
      'vista/template_evaluaciones.xml',
      'vista/template_evaluaciones_detalle.xml',
      'vista/template_historial.xml',
      'vista/template_historial_detalle.xml',
      'vista/template_revision.xml',
      'datos/email_template_analista.xml',
      'vista/template_fecha_evaluacion.xml',
      'vista/template_departamentos.xml',
      'vista/template_dpt_empleados.xml',

            ],
    'installable':True,
    'auto_installable':False,

}
