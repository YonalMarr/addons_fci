#-*- coding:utf-8 -*-
from openerp.osv import fields, osv
from openerp.http import request




class clave_unica(osv.osv):
	_name = 'fci_gh.eva_empleados'
	_inherit = 'fci_gh.eva_empleados'

	_columns = {

        'id_clave':fields.char(
						'Id Codificado',
						size=250,
						required=False,
						help='Clave codificada',
						),
		'f_ure':fields.char(
					'Estatus URE',
		 			size=250,
		 			required=False,
		 			help='Estatus URE',
		 						),
		}
