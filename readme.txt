Modulos de odoo para el sistema de gestion interna para el Consejo Federal de Gobierno.

Modulos:

- Modulo para evaluación del personal del Consejo Federal de Gobierno.
- Modulo para inventario del departamento de sistemas.
- Modulo de seguimiento de archivos para la oficina de seguimiento y control.