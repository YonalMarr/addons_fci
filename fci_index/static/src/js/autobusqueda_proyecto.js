//$(document).ready(function() {
  //  $('#buscador').DataTable();
//} );

// carga las funciones cuando el documento esta listo
$(document).ready(function(){
    // se ejecuta cuando realiza cambios en el input con el ID buscador
    $("#buscador").keyup(function(){
        // Cuando el valor es diferente de vacio
        if( $(this).val() != "")
        {
            // muestra solo los tr que coinciden con el valor introducido en el input
            $("#proyectos tbody>tr").hide();
            $("#proyectos td:contains-ci('" + $(this).val() + "')").parent("tr").show();
        }
        else
        {
            //cuando no hay nada en el input mostrar todos los tr
            $("#proyectos tbody>tr").show();
        }
    });
});
// jQuery expresion para filtrar case-insensitive
$.extend($.expr[":"], 
{
    "contains-ci": function(elem, i, match, array) 
    {
        return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
});
