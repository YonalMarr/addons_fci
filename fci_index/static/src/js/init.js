(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.slider').slider({full_width: true,indicators:false});
    $('ul.tabs').tabs();
    $(".dropdown-button").dropdown();
    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
    $('.parallax').parallax();
	$('.modal-trigger').leanModal({
		dismissible: true, // Modal can be dismissed by clicking outside of the modal
		opacity: .5, // Opacity of modal background
		in_duration: 300, // Transition in duration
		out_duration: 200, // Transition out duration
		//ready: function() { alert('Ready'); }, // Callback for Modal open
		//complete: function() { alert('Closed'); } // Callback for Modal close
		}
	);
      

  }); // end of document ready
})(jQuery); // end of jQuery name space
