# -*- coding: utf-8 -*-

import json
import logging
import base64
from cStringIO import StringIO

import openerp.exceptions
from openerp.addons.web.controllers import main
from werkzeug.exceptions import HTTPException
from openerp import http,tools, api,SUPERUSER_ID
from openerp.http import request
from openerp.addons.website_apiform.controladores import panel, base_tools
from datetime import datetime, date, time, timedelta


_logger = logging.getLogger(__name__)


class principal(main.Home):
    
    @http.route('/', auth='user', website=True)

    def index(self):
        datos={'parametros':{
                    'titulo':'Consejo Federal de Gobierno - Bienvenidos',
                    'template':'fci_index.primera',
                    'url_boton_list':'',
                    'css':'info',
                    },                   
                    
                    }
        return panel.panel_post(datos)


class index_controlador(http.Controller):
    
    @http.route('/inicio', auth='user', website=True)
    def archivo(self):
        datos={'parametros':{
                    'titulo':'Gestión de archivo',
                    'template':'fci_archivo.index',
                    'url_boton_list':'',
                    'css':'info',
                    },                   
                    
                    }
        return panel.panel_post(datos)
      
    
###INDEX
    @http.route('/rrhh', auth='user', website=True)
    def plan_inveraion_redirec(self):
        cr, uid, context = request.cr, request.uid, request.context
        menu_obj = request.registry['ir.ui.menu']
        menu_id=menu_obj.search(cr,SUPERUSER_ID,[('name','=','Employees')])
        url='/web#view_type=kanban&model=hr.employee&menu_id=%d' % menu_id[0]
        return request.redirect(url)
    
    @http.route('/salir', auth='user', website=True)
    def exit(self):
        cr, uid, context = request.cr, request.uid, request.context
        url='/web/session/logout?redirect=/'
        return request.redirect(url)
